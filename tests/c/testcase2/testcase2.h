#pragma once

#include <unity_fixture.h>
#include <verification_api.h>

#include "map_onboard.h"

#define SAMPLE_REG(x) (SAMPLE_CTRL_REG_BASE + (x))
