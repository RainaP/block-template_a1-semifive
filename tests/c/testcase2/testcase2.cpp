/* Copyright 2021 SEMIFIVE, Inc */
/* SPDX-License-Identifier: Apache-2.0 */
#include "testcase2.h"

static volatile int interrupt_callcount=0;
extern "C" void templatewrapper_a1_SAMPLE_TEST2(unsigned int idx) {
	interrupt_callcount++;
	writel(0xC0DE0000 | (interrupt_callcount<<8) | idx, SAMPLE_REG(0));
}

TEST_GROUP(SAMPLE_TEST2);

TEST_SETUP(SAMPLE_TEST2)
{
    // executed before each test
}

TEST_TEAR_DOWN(SAMPLE_TEST2)
{
    // executed after each test
}

TEST(SAMPLE_TEST2, ReadWriteSampleReg0)
{
	uint32_t write_value = 0x01234567;
	writel(write_value, SAMPLE_REG(0));

	TEST_ASSERT_EQUAL(write_value, readl(SAMPLE_REG(0)));
}

TEST(SAMPLE_TEST2, ReadWriteSampleReg4)
{
	uint32_t write_value = 0x89ABCDEF;
	writel(write_value, SAMPLE_REG(4));

	TEST_ASSERT_EQUAL(write_value, readl(SAMPLE_REG(4)));
}

TEST(SAMPLE_TEST2, ReadWriteSampleReg8)
{
	uint32_t write_value_0 = 0x01234567;
	uint32_t write_value_1 = 0x89ABCDEF;
	uint32_t write_value_2 = 0x88888888;

	writel(write_value_2, SAMPLE_REG(8));

	TEST_ASSERT_EQUAL((write_value_0 ^ write_value_1), readl(SAMPLE_REG(8)));
}

TEST(SAMPLE_TEST2, ReadWriteSampleReg12)
{
	uint32_t write_value = 0x3355CCAA;
	writel(write_value, SAMPLE_REG(12));

	TEST_ASSERT_EQUAL(write_value, readl(SAMPLE_REG(12)));
}

TEST(SAMPLE_TEST2, BasicInterrupt)
{
	int timeout=0;
	interrupt_callcount=0;
	metal_interrupt_register_handler(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE, templatewrapper_a1_SAMPLE_TEST2);
	TEST_ASSERT_TRUE( 0 == metal_interrupt_enable(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE) );
    metal_cpu_enable_interrupts();
	writel(~0, SAMPLE_REG(0));
	while(0==interrupt_callcount && timeout<100)	{
		readl(SAMPLE_REG(0));
		SLOG_DEBUG( "Wait for interrupt\n" );
		timeout++;
	}
    metal_cpu_disable_interrupts();
	TEST_ASSERT_TRUE( 0 == metal_interrupt_disable(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE));
	TEST_ASSERT( timeout<100 );
	TEST_ASSERT_EQUAL(0xC0DE0100|SAMPLE_INTERRUPT_BASE, readl(SAMPLE_REG(0)));
}

TEST_GROUP_RUNNER(SAMPLE_TEST2)
{
    RUN_TEST_CASE(SAMPLE_TEST2, ReadWriteSampleReg0);
    RUN_TEST_CASE(SAMPLE_TEST2, ReadWriteSampleReg4);
    RUN_TEST_CASE(SAMPLE_TEST2, ReadWriteSampleReg8);
    RUN_TEST_CASE(SAMPLE_TEST2, ReadWriteSampleReg12);
	RUN_TEST_CASE(SAMPLE_TEST2, BasicInterrupt);
}

TEST_GROUP_CONSTRUCTOR(SAMPLE_TEST2) { REGISTER_TEST_GROUP(SAMPLE_TEST2); }
