#pragma once

#define SAMPLE_CTRL_REG_BASE ((volatile char __iomem *)0x90000000)
#define SAMPLE_DATA_REG_BASE ((volatile char __iomem *)0x90001000)
#define SAMPLE_INTERRUPT_PARENT ((struct metal_interrupt){0})
#define SAMPLE_INTERRUPT_BASE 3
