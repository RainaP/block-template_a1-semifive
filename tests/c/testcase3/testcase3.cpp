/* Copyright 2021 SEMIFIVE, Inc */
/* SPDX-License-Identifier: Apache-2.0 */
#include "testcase3.h"

static volatile int interrupt_callcount=0;
extern "C" void templatewrapper_a1_SAMPLE_TEST3(unsigned int idx) {
	interrupt_callcount++;
	writel(0xC0DE0000 | (interrupt_callcount<<8) | idx, SAMPLE_REG(0));
}

TEST_GROUP(SAMPLE_TEST3);

TEST_SETUP(SAMPLE_TEST3)
{
    // executed before each test
}

TEST_TEAR_DOWN(SAMPLE_TEST3)
{
    // executed after each test
}

TEST(SAMPLE_TEST3, ReadWriteSampleReg0)
{
	uint64_t write_value = 0x0123456789ABCDEF;
	writeq(write_value, SAMPLE_REG(0));

	TEST_ASSERT_EQUAL(write_value, readq(SAMPLE_REG(0)));
}

TEST(SAMPLE_TEST3, ReadWriteSampleReg4)
{
	uint64_t write_value = 0xFEDCBA9876543210;
	writeq(write_value, SAMPLE_REG(8));

	TEST_ASSERT_EQUAL(write_value, readq(SAMPLE_REG(8)));
}

TEST(SAMPLE_TEST3, ReadWriteSampleReg8)
{
	uint64_t write_value = 0xCCAA33553355CCAA;

	writeq(write_value, SAMPLE_REG(16));

	TEST_ASSERT_EQUAL(write_value, readq(SAMPLE_REG(16)));
}

TEST(SAMPLE_TEST3, ReadWriteSampleReg12)
{
	uint64_t write_value = 0xAACC55335533AACC;

	writeq(write_value, SAMPLE_REG(24));

	TEST_ASSERT_EQUAL(write_value, readq(SAMPLE_REG(24)));
}

TEST(SAMPLE_TEST3, BasicInterrupt)
{
	int timeout=0;
	interrupt_callcount=0;
	metal_interrupt_register_handler(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE, templatewrapper_a1_SAMPLE_TEST3);
	TEST_ASSERT_TRUE( 0 == metal_interrupt_enable(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE) );
    metal_cpu_enable_interrupts();
	writel(~0, SAMPLE_REG(0));
	while(0==interrupt_callcount && timeout<100)	{
		readq(SAMPLE_REG(0));
		SLOG_DEBUG( "Wait for interrupt\n" );
		timeout++;
	}
    metal_cpu_disable_interrupts();
	TEST_ASSERT_TRUE( 0 == metal_interrupt_disable(SAMPLE_INTERRUPT_PARENT,SAMPLE_INTERRUPT_BASE));
	TEST_ASSERT( timeout<100 );
	TEST_ASSERT_EQUAL(0xC0DE0100|SAMPLE_INTERRUPT_BASE, readl(SAMPLE_REG(0)));
}

TEST_GROUP_RUNNER(SAMPLE_TEST3)
{
    RUN_TEST_CASE(SAMPLE_TEST3, ReadWriteSampleReg0);
    RUN_TEST_CASE(SAMPLE_TEST3, ReadWriteSampleReg4);
    RUN_TEST_CASE(SAMPLE_TEST3, ReadWriteSampleReg8);
    RUN_TEST_CASE(SAMPLE_TEST3, ReadWriteSampleReg12);
//	RUN_TEST_CASE(SAMPLE_TEST3, BasicInterrupt);
}

TEST_GROUP_CONSTRUCTOR(SAMPLE_TEST3) { REGISTER_TEST_GROUP(SAMPLE_TEST3); }
