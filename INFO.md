## Repository information

  1. Repository name      : block-template-a1-semifive
  2. IP name              : template type a1 (1 apb slave, 4 interrupt outputs)
  3. Repository commit id : 
  4. Origin commit id     : 
  5. Tool version info    
            a) wit        : 0.14.1
            b) wake       : 0.20.1
            c) duh        : 
            d) python     : 3.7.1
            e) riscv tool : 2019.08.0

