// Generated Code
// Please DO NOT EDIT


package semifive.blocks.templatewrapper

import chisel3._
// import chisel3.{withClockAndReset, _}
import chisel3.util._
import chisel3.experimental._

import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.diplomaticobjectmodel.{DiplomaticObjectModelAddressing, HasLogicalTreeNode}
import freechips.rocketchip.diplomaticobjectmodel.logicaltree.{LogicalTreeNode, LogicalModuleTree}
import freechips.rocketchip.diplomaticobjectmodel.model._
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.amba.apb._
import freechips.rocketchip.amba.ahb._
import freechips.rocketchip.interrupts._
import freechips.rocketchip.prci._
import freechips.rocketchip.util.{ElaborationArtefacts}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.subsystem._
import freechips.rocketchip.regmapper._

import sifive.blocks.util.{NonBlockingEnqueue, NonBlockingDequeue}

import semifive.rocketbooster.devices.amba.axi4.SCUCRGAttachParams


class templatewrapperBlackBoxIO(

) extends Bundle {
  val CLK__Control          = Input(Bool())
  val RSTN__APB__presetn    = Input(Bool())
  val CLK__Data             = Input(Bool())
  val RSTN__AXI4__presetn   = Input(Bool())

  val APB__Control__PADDR   = Input(UInt((32).W))
  val APB__Control__PSEL    = Input(Bool())
  val APB__Control__PENABLE = Input(Bool())
  val APB__Control__PWRITE  = Input(Bool())
  val APB__Control__PWDATA  = Input(UInt((32).W))
  val APB__Control__PRDATA  = Output(UInt((32).W))
  val APB__Control__PCLKEN  = Input(Bool())
  val APB__Control__PPROT   = Input(UInt((4).W))
  val APB__Control__PSTRB   = Input(UInt((4).W))
  val APB__Control__PREADY  = Output(Bool())
  val APB__Control__PSLVERR = Output(Bool())
  val AXI4__Data__AWVALID   = Input(Bool())
  val AXI4__Data__AWADDR    = Input(UInt((32).W))
  val AXI4__Data__AWPROT    = Input(UInt((3).W))
  val AXI4__Data__AWID      = Input(UInt((4).W))
  val AXI4__Data__AWLEN     = Input(UInt((8).W))
  val AXI4__Data__AWSIZE    = Input(UInt((3).W))
  val AXI4__Data__AWBURST   = Input(UInt((2).W))
  val AXI4__Data__AWLOCK    = Input(Bool())
  val AXI4__Data__AWCACHE   = Input(UInt((4).W))
  val AXI4__Data__AWQOS     = Input(UInt((4).W))
  val AXI4__Data__AWREGION  = Input(UInt((4).W))
  val AXI4__Data__AWUSER    = Input(Bool())
  val AXI4__Data__AWREADY   = Output(Bool())
  val AXI4__Data__WDATA     = Input(UInt((64).W))
  val AXI4__Data__WSTRB     = Input(UInt((16).W))
  val AXI4__Data__WLAST     = Input(Bool())
  val AXI4__Data__WVALID    = Input(Bool())
  val AXI4__Data__WREADY    = Output(Bool())
  val AXI4__Data__BREADY    = Input(Bool())
  val AXI4__Data__BID       = Output(UInt((4).W))
  val AXI4__Data__BRESP     = Output(UInt((2).W))
  val AXI4__Data__BUSER     = Output(Bool())
  val AXI4__Data__BVALID    = Output(Bool())
  val AXI4__Data__ARVALID   = Input(Bool())
  val AXI4__Data__ARADDR    = Input(UInt((32).W))
  val AXI4__Data__ARPROT    = Input(UInt((3).W))
  val AXI4__Data__ARID      = Input(UInt((4).W))
  val AXI4__Data__ARLEN     = Input(UInt((8).W))
  val AXI4__Data__ARSIZE    = Input(UInt((3).W))
  val AXI4__Data__ARBURST   = Input(UInt((2).W))
  val AXI4__Data__ARLOCK    = Input(Bool())
  val AXI4__Data__ARCACHE   = Input(UInt((4).W))
  val AXI4__Data__ARQOS     = Input(UInt((4).W))
  val AXI4__Data__ARREGION  = Input(UInt((4).W))
  val AXI4__Data__ARUSER    = Input(Bool())
  val AXI4__Data__ARREADY   = Output(Bool())
  val AXI4__Data__RID       = Output(UInt((4).W))
  val AXI4__Data__RDATA     = Output(UInt((64).W))
  val AXI4__Data__RRESP     = Output(UInt((2).W))
  val AXI4__Data__RLAST     = Output(Bool())
  val AXI4__Data__RUSER     = Output(Bool())
  val AXI4__Data__RVALID    = Output(Bool())
  val AXI4__Data__RREADY    = Input(Bool())

  val IRQ__t0               = Output(Bool())
  val IRQ__t1               = Output(Bool())
  val IRQ__t2               = Output(Bool())
  val IRQ__t3               = Output(Bool())

  val IO__tOEN              = Output(UInt((32).W))
  val IO__tOUT              = Output(UInt((32).W))
  val IO__tIN               = Input(UInt((32).W))
  val IO__tIO               = (Analog((32).W))
}

class templatewrapper(

) extends BlackBox(Map(

)) with HasBlackBoxResource {
  val io = IO(new templatewrapperBlackBoxIO(

  ))
  addResource("/template.sv")
  addResource("/busSubordinateModel.sv")
  addResource("/axi4SubordinateModel.sv")
  addResource("/templatewrapper.sv")
}

case class templatewrapperParams(
  apbSlaveParams: PapbSlaveParams,
  axi4SlaveParams: Paxi4SlaveParams,
  irqParams: PirqParams,
  cacheBlockBytes: Int,
  crgAttachParams: SCUCRGAttachParams
)

// busType: APB4, mode: slave
// busType: AXI4, mode: slave
// busType: interrupts, mode: master

class LtemplatewrapperBase(c: templatewrapperParams)(implicit p: Parameters) extends LazyModule {

  def extraResources(resources: ResourceBindings) = Map[String, Seq[ResourceValue]]()

  val device = new SimpleDevice("templatewrapper", Seq("semifive,templatewrapper-0.1.0")) {
    override def describe(resources: ResourceBindings): Description = {
      val Description(name, mapping) = super.describe(resources)
      Description(name, mapping ++ extraResources(resources))
    }
  }

  val clockNode = ClockSinkNode(Seq(c.crgAttachParams.clockParams))

  val apbSlaveNode = APBSlaveNode(Seq(
    APBSlavePortParameters(
      slaves = Seq(APBSlaveParameters(
        address       = List(AddressSet(c.apbSlaveParams.base, ((1L << 12) - 1))),
        resources     = device.reg("templatewrapper_ctrl"),
        // regionType
        executable    = false,
        // nodePath
        supportsWrite = true,
        supportsRead  = true
        // device
      )),
      beatBytes = 32 / 8
    )
  ))

  val axi4SlaveNode = AXI4SlaveNode(Seq(
    AXI4SlavePortParameters(
      slaves = Seq(AXI4SlaveParameters(
        address       = List(AddressSet(c.axi4SlaveParams.base, ((1L << 12) - 1))),
        resources = device.reg("templatewrapper_data"),
        // regionType
        executable    = c.axi4SlaveParams.executable,
        // nodePath
        supportsWrite = TransferSizes(1, (64 * 1 / 8)),
        supportsRead  = TransferSizes(1, (64 * 1 / 8)),
        interleavedId = Some(0),
        // device
        )
      ),
      beatBytes = 64 / 8
    )
  ))

  val irqNode = IntSourceNode(IntSourcePortSimple(
    num = 4,
    resources = device.int
  ))

  val ioBridgeSource = BundleBridgeSource(() => new templatewrapperVipIO(

  ))

  class LtemplatewrapperBaseImp extends LazyRawModuleImp(this) {
    val blackbox = Module(new templatewrapper(

    ))
    // clock/reset wiring
    blackbox.io.CLK__Control         :=  clockNode.in.head._1.clock.asBool()
    blackbox.io.RSTN__APB__presetn   := ~clockNode.in.head._1.reset.asBool()
    blackbox.io.CLK__Data            :=  clockNode.in.head._1.clock.asBool()
    blackbox.io.RSTN__AXI4__presetn  := ~clockNode.in.head._1.reset.asBool()
    // bus interface wiring
    //   bus interface alias
    val apbSlave0   =  apbSlaveNode.in(0)._1
    val axi4Slave0  =  axi4SlaveNode.in(0)._1
    val irq0        =  irqNode.out(0)._1
    //   wiring for apbSlave of type APB4
    blackbox.io.APB__Control__PADDR    :=  apbSlave0.paddr
    blackbox.io.APB__Control__PSEL     :=  apbSlave0.psel
    blackbox.io.APB__Control__PENABLE  :=  apbSlave0.penable
    blackbox.io.APB__Control__PPROT    :=  apbSlave0.pprot
    blackbox.io.APB__Control__PWRITE   :=  apbSlave0.pwrite
    blackbox.io.APB__Control__PWDATA   :=  apbSlave0.pwdata
    apbSlave0.prdata                   :=  blackbox.io.APB__Control__PRDATA
    apbSlave0.pready                   :=  blackbox.io.APB__Control__PREADY
    apbSlave0.pslverr                  :=  blackbox.io.APB__Control__PSLVERR
    //   wiring for axi4Slave of type AXI4
    //      AW
    blackbox.io.AXI4__Data__AWVALID  :=  axi4Slave0.aw.valid
    blackbox.io.AXI4__Data__AWADDR   :=  axi4Slave0.aw.bits.addr
    blackbox.io.AXI4__Data__AWPROT   :=  axi4Slave0.aw.bits.prot
    blackbox.io.AXI4__Data__AWID     :=  axi4Slave0.aw.bits.id
    blackbox.io.AXI4__Data__AWLEN    :=  axi4Slave0.aw.bits.len
    blackbox.io.AXI4__Data__AWSIZE   :=  axi4Slave0.aw.bits.size
    blackbox.io.AXI4__Data__AWBURST  :=  axi4Slave0.aw.bits.burst
    blackbox.io.AXI4__Data__AWLOCK   :=  false.B
    blackbox.io.AXI4__Data__AWCACHE  :=  axi4Slave0.aw.bits.cache
    blackbox.io.AXI4__Data__AWQOS    :=  axi4Slave0.aw.bits.qos
    blackbox.io.AXI4__Data__AWREGION :=  0.U
    blackbox.io.AXI4__Data__AWUSER   :=  false.B
    axi4Slave0.aw.ready              :=  blackbox.io.AXI4__Data__AWREADY
    //      W
    blackbox.io.AXI4__Data__WDATA    :=  axi4Slave0.w.bits.data
    blackbox.io.AXI4__Data__WSTRB    :=  axi4Slave0.w.bits.strb
    blackbox.io.AXI4__Data__WLAST    :=  true.B
    blackbox.io.AXI4__Data__WVALID   :=  axi4Slave0.w.valid
    axi4Slave0.w.ready               :=  blackbox.io.AXI4__Data__WREADY
    //      B
    blackbox.io.AXI4__Data__BREADY   :=  axi4Slave0.b.ready
    axi4Slave0.b.bits.id             :=  blackbox.io.AXI4__Data__BID
    axi4Slave0.b.bits.resp           :=  blackbox.io.AXI4__Data__BRESP
    axi4Slave0.b.valid               :=  blackbox.io.AXI4__Data__BVALID
    //      AR
    blackbox.io.AXI4__Data__ARVALID  :=  axi4Slave0.ar.valid
    blackbox.io.AXI4__Data__ARADDR   :=  axi4Slave0.ar.bits.addr
    blackbox.io.AXI4__Data__ARPROT   :=  axi4Slave0.ar.bits.prot
    blackbox.io.AXI4__Data__ARID     :=  axi4Slave0.ar.bits.id
    blackbox.io.AXI4__Data__ARLEN    :=  axi4Slave0.ar.bits.len
    blackbox.io.AXI4__Data__ARSIZE   :=  axi4Slave0.ar.bits.size
    blackbox.io.AXI4__Data__ARBURST  :=  axi4Slave0.ar.bits.burst
    blackbox.io.AXI4__Data__ARLOCK   :=  false.B
    blackbox.io.AXI4__Data__ARCACHE  :=  axi4Slave0.ar.bits.cache
    blackbox.io.AXI4__Data__ARQOS    :=  axi4Slave0.ar.bits.qos
    blackbox.io.AXI4__Data__ARREGION :=  0.U
    blackbox.io.AXI4__Data__ARUSER   :=  false.B
    axi4Slave0.ar.ready              :=  blackbox.io.AXI4__Data__ARREADY
    //      R
    axi4Slave0.r.bits.id             :=  blackbox.io.AXI4__Data__RID
    axi4Slave0.r.bits.data           :=  blackbox.io.AXI4__Data__RDATA
    axi4Slave0.r.bits.resp           :=  blackbox.io.AXI4__Data__RRESP
    axi4Slave0.r.bits.last           :=  blackbox.io.AXI4__Data__RLAST
//  axi4Slave0.r.bits.user           :=  blackbox.io.AXI4__Data__RUSER
    axi4Slave0.r.valid               :=  blackbox.io.AXI4__Data__RVALID
    blackbox.io.AXI4__Data__RREADY   :=  axi4Slave0.r.ready
    // irq wiring
    irq0(0)  :=  blackbox.io.IRQ__t0
    irq0(1)  :=  blackbox.io.IRQ__t1
    irq0(2)  :=  blackbox.io.IRQ__t2
    irq0(3)  :=  blackbox.io.IRQ__t3
    // i/o wiring
    ioBridgeSource.bundle.IO__tOEN   :=  blackbox.io.IO__tOEN
    ioBridgeSource.bundle.IO__tOUT   :=  blackbox.io.IO__tOUT
    blackbox.io.IO__tIN              :=  ioBridgeSource.bundle.IO__tIN
    ioBridgeSource.bundle.IO__tIO    <>  blackbox.io.IO__tIO
  }
  lazy val module = new LtemplatewrapperBaseImp
}

case class PapbSlaveParams(
  base: BigInt,
  executable: Boolean = false,
  maxFifoBits: Int = 2,
  maxTransactions: Int = 1,
  tlBufferParams: TLBufferParams = TLBufferParams()
)

case class Paxi4SlaveParams(
  base: BigInt,
  executable: Boolean = false,
  maxFifoBits: Int = 2,
  maxTransactions: Int = 1,
  axi4BufferParams: AXI4BufferParams = AXI4BufferParams(),
  tlBufferParams: TLBufferParams = TLBufferParams()
)

case class PirqParams()

case class NtemplatewrapperTopParams(
  blackbox: templatewrapperParams
) {
  def setBurstBytes(x: Int): NtemplatewrapperTopParams = this.copy()
}

object NtemplatewrapperTopParams {
  def defaults(
    apbSlave_base: BigInt,
    axi4Slave_base: BigInt,
    cacheBlockBytes: Int,
    crgAttachParams: SCUCRGAttachParams
  ) = NtemplatewrapperTopParams(
    blackbox = templatewrapperParams(
      apbSlaveParams = PapbSlaveParams(base = apbSlave_base),
      axi4SlaveParams = Paxi4SlaveParams(base = axi4Slave_base),
      irqParams = PirqParams(),
      cacheBlockBytes = cacheBlockBytes,
      crgAttachParams = crgAttachParams
    )
  )
}


class NtemplatewrapperTopLogicalTreeNode(component: NtemplatewrapperTopBase) extends LogicalTreeNode(() => Some(component.imp.device)) {
  override def getOMComponents(resourceBindings: ResourceBindings, components: Seq[OMComponent]): Seq[OMComponent] = {
    val name = component.imp.device.describe(resourceBindings).name
    val omDevice = new scala.collection.mutable.LinkedHashMap[String, Any] with OMDevice {
      val memoryRegions = component.getOMMemoryRegions(resourceBindings)
      val interrupts = component.getOMInterrupts(resourceBindings)
      val _types: Seq[String] = Seq("OMtemplatewrapper", "OMDevice", "OMComponent", "OMCompoundType")
    }
    val userOM = component.userOM
    val values = userOM.productIterator
    if (values.nonEmpty) {
      val pairs = (userOM.getClass.getDeclaredFields.map { field =>
        assert(field.getName != "memoryRegions", "user Object Model must not define \"memoryRegions\"")
        assert(field.getName != "interrupts", "user Object Model must not define \"interrupts\"")
        assert(field.getName != "_types", "user Object Model must not define \"_types\"")

        field.getName -> values.next
      }).toSeq
      omDevice ++= pairs
    }
    omDevice("memoryRegions") = omDevice.memoryRegions
    omDevice("interrupts") = omDevice.interrupts
    omDevice("_types") = omDevice._types
    Seq(omDevice)
  }
}

class NtemplatewrapperTopBase(val c: NtemplatewrapperTopParams)(implicit p: Parameters)
 extends SimpleLazyModule
 with BindingScope
 with HasLogicalTreeNode {
  val imp = LazyModule(new Ltemplatewrapper(c.blackbox))

  ResourceBinding { Resource(imp.device, "exists").bind(ResourceString("yes")) }

  def userOM: Product with Serializable = Nil

  private def padFields(fields: (Int, RegField)*) = {
    var previousOffset = 0
    var previousField: Option[RegField] = None

    fields.flatMap { case (fieldOffset, field) =>
      val padWidth = fieldOffset - previousOffset
      require(padWidth >= 0,
        if (previousField.isDefined) {
          s"register fields at $previousOffset and $fieldOffset are overlapping"
        } else {
          s"register field $field has a negative offset"
        })

      previousOffset = fieldOffset
      previousField = Some(field)

      if (padWidth > 0) {
        Seq(RegField(padWidth), field)
      } else {
        Seq(field)
      }
    }
  }
  def omRegisterMaps = Seq(
    OMRegister.convert(
      0    -> RegFieldGroup("ctrl_register_0", Some("""This register for controler registger0"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_0", "")))),
      4    -> RegFieldGroup("ctrl_register_1", Some("""This register for controler registger1"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_1", "")))),
      8    -> RegFieldGroup("ctrl_register_2", Some("""This register for controler registger2"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_2", "")))),
      12   -> RegFieldGroup("ctrl_register_3", Some("""This register for controler registger3"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_3", "")))),
      4096 -> RegFieldGroup("data_register_0", Some("""This register for data registger0"""),      padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_0", "")))),
      4100 -> RegFieldGroup("data_register_1", Some("""This register for data registger1"""),      padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_1", "")))),
      4104 -> RegFieldGroup("data_register_2", Some("""This register for data registger2"""),      padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_2", "")))),
      4108 -> RegFieldGroup("data_register_3", Some("""This register for data registger3"""),      padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_3", ""))))
    )
  )
/*
  def omRegisterMaps: Map[String, OMRegisterMap] = Map(
    "Control_Register" -> OMRegister.convertSeq(
      RegFieldAddressBlock(
        AddressBlockInfo(
          "controlAddressBlock",
          addressOffset = 0,
          range = 4096,
          width = 32,
        ),
//      addAddressoffset = true,
        0  -> RegFieldGroup("ctrl_register_0", Some("""This register for controler registger0"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_0", "")))),
        4  -> RegFieldGroup("ctrl_register_1", Some("""This register for controler registger1"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_1", "")))),
        8  -> RegFieldGroup("ctrl_register_2", Some("""This register for controler registger2"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_2", "")))),
        12 -> RegFieldGroup("ctrl_register_3", Some("""This register for controler registger3"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("ctrl_register_3", "")))),
      )
    ),
    "Data_Register" -> OMRegister.convertSeq(
      RegFieldAddressBlock(
        AddressBlockInfo(
          "dataAddressBlock",
          addressOffset = 4096,
          range = 4096,
          width = 32,
        ),
//      addAddressoffset = true,
      0  -> RegFieldGroup("data_register_0", Some("""This register for data registger0"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_0", "")))),
      4  -> RegFieldGroup("data_register_1", Some("""This register for data registger1"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_1", "")))),
      8  -> RegFieldGroup("data_register_2", Some("""This register for data registger2"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_2", "")))),
      12 -> RegFieldGroup("data_register_3", Some("""This register for data registger3"""), padFields( 0 -> RegField(32, Bool(), RegFieldDesc("data_register_3", ""))))
      )
    )
  )
*/
  def getOMMemoryRegions(resourceBindings: ResourceBindings): Seq[OMMemoryRegion] = {
    val name = imp.device.describe(resourceBindings).name
    DiplomaticObjectModelAddressing.getOMMemoryRegions(name, resourceBindings, None)
  }

  def getOMInterrupts(resourceBindings: ResourceBindings): Seq[OMInterrupt] = {
    val name = imp.device.describe(resourceBindings).name
    DiplomaticObjectModelAddressing.describeGlobalInterrupts(name, resourceBindings)
  }

  def logicalTreeNode: LogicalTreeNode = new NtemplatewrapperTopLogicalTreeNode(this)


// no channel node

  val apbSlaveNode: APBSlaveNode = imp.apbSlaveNode
  val axi4SlaveNode: AXI4SlaveNode = imp.axi4SlaveNode
  val irqNode: IntSourceNode = imp.irqNode
}
