// USER editable file


package semifive.blocks.templatewrapper

import chisel3._
// import chisel3.{withClockAndReset, _}
import chisel3.util._
import chisel3.experimental._

import freechips.rocketchip.config._
import freechips.rocketchip.diplomacy._
import freechips.rocketchip.diplomaticobjectmodel.{DiplomaticObjectModelAddressing, HasLogicalTreeNode}
import freechips.rocketchip.diplomaticobjectmodel.logicaltree.{LogicalTreeNode, LogicalModuleTree}
import freechips.rocketchip.diplomaticobjectmodel.model._
import freechips.rocketchip.amba.axi4._
import freechips.rocketchip.amba.apb._
import freechips.rocketchip.amba.ahb._
import freechips.rocketchip.interrupts._
import freechips.rocketchip.prci._
import freechips.rocketchip.util.{ElaborationArtefacts}
import freechips.rocketchip.tilelink._
import freechips.rocketchip.subsystem._
import freechips.rocketchip.regmapper._
import freechips.rocketchip.devices.tilelink.DevNullParams

import sifive.blocks.util.{NonBlockingEnqueue, NonBlockingDequeue}

import semifive.rocketbooster.subsystem._
import semifive.rocketbooster.amba.axi4._
import semifive.rocketbooster.amba.apb._
import semifive.rocketbooster.interrupt._
import semifive.rocketbooster.devices.amba.axi4._
import semifive.rocketbooster.util.Site
import semifive.skeleton.SkeletonSoC
import semifive.skeleton.config.SkeletonSoCKey
import semifive.skeleton.config.onboardsystem._
import semifive.vip.loopback._

import scala.collection.mutable.{LinkedHashMap, LinkedHashSet}

// IO types
//    templatewrapperVipIO       : will be connected to 'specific vip'
//    templatewrapperDftIjtagIO  : will be connected to 'ijtag wrapper'
//    templatewrapperDftTopmuxIO : will be connected to 'top level pad mux'
//    templatewrapperDftDaIO     : will be connected to 'primary port'

// These pins are connected to the specific VIP which used for IP verification
class templatewrapperVipIO(

) extends Bundle {
  val IO__tOEN = Output(UInt((32).W))
  val IO__tOUT = Output(UInt((32).W))
  val IO__tIN = Input(UInt((32).W))
  val IO__tIO = (Analog((32).W))
}

class Ltemplatewrapper(c: templatewrapperParams)(implicit p: Parameters) extends LtemplatewrapperBase(c)(p)
{

// User code here

}

class NtemplatewrapperTop(c: NtemplatewrapperTopParams)(implicit p: Parameters) extends NtemplatewrapperTopBase(c)(p)
{

// User code here
  // route the ports of the black box to this sink
  val ioBridgeSink = BundleBridgeSink[templatewrapperVipIO]()
  ioBridgeSink := imp.ioBridgeSource

  // create a new ports to this source
  // !!!!  Top level integrator will determine where to hook-up these nodes  !!!!
  val templatewrapperVipNode    = BundleBridgeSource(() => new templatewrapperVipIO())

  // logic to connect ioBridgeSink/Source nodes
  override lazy val module = new LazyModuleImp(this) {

    // source -> sink
    ioBridgeSink.bundle.IO__tIN := templatewrapperVipNode.bundle.IO__tIN
    ioBridgeSink.bundle.IO__tIO <> templatewrapperVipNode.bundle.IO__tIO

    // source <- sink
    templatewrapperVipNode.bundle.IO__tOEN := ioBridgeSink.bundle.IO__tOEN
    templatewrapperVipNode.bundle.IO__tOUT := ioBridgeSink.bundle.IO__tOUT
  }
}

/********************/
/* SOC TOPOLOGY API */
/********************/
case class TemplateWrapperSite(i: Int) extends Site[NtemplatewrapperTop](s"TemplateWrapper_$i")

case class TemplateWrapperPlaceParams(
    key: TemplateWrapperSite,
    params: NtemplatewrapperTopParams,
    controlWhere: APBBusSite,
    dataWhere: AXI4BusSite,
    interruptWhere: InterruptBusSite)
  extends DevicePlaceParams[NtemplatewrapperTop] {

  def instantiate(where: SystemView)(implicit p: Parameters) = where {
    val templateWrapper = LazyModule(new NtemplatewrapperTop(params))
    templateWrapper.imp.suggestName(key.name)
    templateWrapper
  }

  def connect(where: SystemView)(implicit p: Parameters) = {
    val templateWrapper = where.subscribeIn(key)
    where.subscribeIn(controlWhere).coupleTo(key.name) { templateWrapper.apbSlaveNode := _}
    where.subscribeIn(dataWhere).coupleTo(key.name) { templateWrapper.axi4SlaveNode := _ }
    where.subscribeIn(interruptWhere).coupleFrom(key.name) { _ := templateWrapper.irqNode }
    where.underClockgroup(templateWrapper.imp.clockNode, params.blackbox.crgAttachParams)
  }
}

class TemplateSoC(harness: LazyScope)(implicit p: Parameters) extends SkeletonSoC(harness) {

  val templateSystem = subscribeIn(InOnboardSystem)
  val templateWrapper = subscribeWith(TemplateWrapperSite(0), InOnboardSystem)

  harness {
    // instantiate the vip
    val vip = LazyModule(new NloopbackTop(NloopbackTopParams(blackbox = loopbackParams(cacheBlockBytes = p(CacheBlockBytes)))))

    // route vip signals to the testharness
    val vipNode = vip.imp.ioBridgeSource.makeSink()

    // route templatewrapper signals to the testharness
    val templateWrapperNode = templateWrapper.templatewrapperVipNode.makeSink()

    // connect the templatewrapper and vip signals
    InModuleBody {
      vipNode.bundle.oenable             := templateWrapperNode.bundle.IO__tOEN
      vipNode.bundle.odata               := templateWrapperNode.bundle.IO__tOUT
      templateWrapperNode.bundle.IO__tIN := vipNode.bundle.idata
      templateWrapperNode.bundle.IO__tIO <> vipNode.bundle.iodata
    }
  }
}

class WithTemplateWrapper extends Config((site, here, up) => {
  case DeviceTopologySite(InOnboardSystem) =>
    DeviceTopology(
      place = Seq(
        TemplateWrapperPlaceParams(
          key = TemplateWrapperSite(0),
          params = NtemplatewrapperTopParams.defaults(0x90000000L, 0x90001000L, site(CacheBlockBytes), SCUCRGAttachParams("templatewrapper", 50, Some("inbound_IP"))),
          controlWhere = APBCBUS,
          dataWhere = AXI4CBUS,
          interruptWhere = IBUS))
    ) +: up(DeviceTopologySite(InOnboardSystem))
})

class WithTemplateWrapperDefault extends Config((site, here, up) => {
  case SkeletonSoCKey => { (s: LazyScope, p: Parameters) => new TemplateSoC(s)(p) }
})

class WithtemplatewrapperSoCOnboarding extends Config(
  new WithTemplateWrapper ++
  new WithTemplateWrapperDefault ++
  new WithAPBFanoutBusSlaveInOnboardSystem() ++
  new DefaultOnboardSystemConfig
)
