// ############################################################################
//
// Generic AXI4 Subordinate Model
//
//  Written by kieran.kim@semifive.com
//
// Brief description
//  module that implements AXI4 representation of busSubordinateModel
//
// ############################################################################
//
// TODO
//  2021/04/11  (kieran.kim)
//      Add     support for wrapping burst transactinos
//      Add     support for fixed burst
//      Add     support for exclusive transactions
//      Add     support for access permission control
//      Add     mechanism to inject error
//      Add     support for big-endian
//
//  2021/04/13  (kieran.kim)
//      Enhance nxtWready asserting mechanism (randomize)
//      Add     shuffling mechanism on read data among different IDs
//
// ############################################################################
//
// RELEASE NOTES
//  2021/08/05  (kieran.kim)
//      Fix     narrow access
//
//  2021/04/22  (kieran.kim)
//      Add     backdoor access
//
//  2021/04/11  (kieran.kim)
//      Init
//
// ############################################################################
//
// Implementation Note
//
// [NOT Supported Features]
//  The following features are NOT supported yet:
//    - fixed burst
//    - wrapping burst
//    - AxLOCK
//    - AxCACHE (no meaning at sink)
//    - AxPROT
//    - AxQOS   (no meaning at sink)
//    - AxREGION(no meaning at sink)
//    - Scrambling read data bursts among multiple IDs
//
// [User Define Extension Ports]
//  They must be categorized into two groups
//
//  One is awuser & buser; their bit width is defined by WIDTH_WUSER
//  The other one is aruser & ruser; their bit width is defined by WIDTH_RUSER
//  A port named wuser is NOT supported
//
//  A value that is transferred on awuser will be shown on buser of the
//  corresponding beat on B channel; aruser to ruser works in the same manner
//
// [Backdoor Accessibility]
//    backdoorWr: backdoor write
//      longint                bAddr        //Byte address to access
//      byte                   data         //write data
//
//    backdoorRd: backdoor read returning 8-bit
//      longint                bAddr        //Byte address to access
//
// ############################################################################
//

module axi4SubordinateModel #(
//AMBA
parameter WIDTH_WID       = 4,            //bit width of AW & B channel ID
parameter WIDTH_RID       = 4,            //bit width of AR & R channel ID
parameter WIDTH_ADDR      = 32,           //bit width of address
parameter WIDTH_DATA      = 128,          //bit width of data
parameter WIDTH_STRB      = WIDTH_DATA/8, //bit width of data mask (MNEMONIC)

parameter WIDTH_WUSER     = 1,            //bit width of awuser & buser
parameter WIDTH_RUSER     = 1,            //bit width of aruser & ruser

//system topology
parameter REGION_BASE     = 'h0,          //starting address allocated for this module
parameter REGION_SIZE     = 'h100000,     //size of address region allocated for this module

//throughput
parameter OUTSTAND_WR     = 16,           //max num of outstandings for write
parameter OUTSTAND_RD     = 16,           //max num of outstandings for read

//arbitration
parameter TYPE_ARB_WR     = "FCFS",       //arbitration type for write ID
                                          //  "FCFS", "RAND" or "RR"
parameter TYPE_ARB_RD     = "FCFS",       //arbitration type for  read ID
                                          //  "FCFS", "RAND" or "RR"

//latency
parameter TYPE_LATENCY_WR = "UNIFORM",    //latency model for write
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
parameter LAT_WR_MAX_DEV  = 4,            //latency model param for write
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
parameter LAT_WR_MIN_AVG  = 4,            //latency model param for write
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

parameter TYPE_LATENCY_RD = "UNIFORM",    //latency model for read
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
parameter LAT_RD_MAX_DEV  = 4,            //latency model param for read
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
parameter LAT_RD_MIN_AVG  = 4,            //latency model param for read
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

//logging
parameter MAX_MESSAGE     = 1000,         //max num of warning/error messages
parameter EN_WAR_CHECK    = 1             //0: disable read-prior-to-write check (Write-After-Read)
)
(
//AW channel
input  logic [WIDTH_WID  -1:0] awid,      //identification tag for a write transaction
input  logic [WIDTH_ADDR -1:0] awaddr,    //address of the first transfer in a write transaction
input  logic [          8-1:0] awlen,     //exact number of data transfers in a write transaction
input  logic [          3-1:0] awsize,    //num of Bytes in each data transfer in a write transaction
input  logic [          2-1:0] awburst,   //burst type
input  logic                   awlock,    //atomic characteristics of a write transaction
input  logic [          4-1:0] awcache,   //(DANGLE)indicates how a write transaction is required to progress through a system
input  logic [          3-1:0] awprot,    //protection attributes of a write transaction
input  logic [          4-1:0] awqos,     //(DANGLE)quality of Service identifier for a write transaction
input  logic [          4-1:0] awregion,  //(DANGLE)region indicator for a write transaction
input  logic [WIDTH_WUSER-1:0] awuser,    //user-defined extension for the write address channel
input  logic                   awvalid,   //indicates that the write address channel signals are valid
output logic                   awready,   //indicates that a transfer on the write address channel can be accepted

// W channel
input  logic [WIDTH_DATA -1:0] wdata,     //write data
input  logic [WIDTH_STRB -1:0] wstrb,     //write strobe, indicate which Byte lanes hold valid data
input  logic                   wlast,     //indicates whether this is the last data transfer in a write transaction
input  logic                   wvalid,    //indicates that the write data channel signals are valid
output logic                   wready,    //indicates that a transfer on the write data channel can be accepted

// B channel
output logic [WIDTH_WID  -1:0] bid,       //identification tag for a write reponse
output logic [          2-1:0] bresp,     //write response, indicates the status of a write transaction
output logic [WIDTH_WUSER-1:0] buser,     //user-defined extension for the write reponse channel
output logic                   bvalid,    //indicates that the write response channel signals are valid
input  logic                   bready,    //indicates that a transfer on the write reponse channel can be accepted

//AR channel
input  logic [WIDTH_RID  -1:0] arid,      //identification tag for a read transaction
input  logic [WIDTH_ADDR -1:0] araddr,    //address of the first transfer in a read transaction
input  logic [          8-1:0] arlen,     //exact number of data transfers in a read transaction
input  logic [          3-1:0] arsize,    //num of Bytes in each data transfer in a read transaction
input  logic [          2-1:0] arburst,   //burst type
input  logic                   arlock,    //atomic characteristics of a read transaction
input  logic [          4-1:0] arcache,   //(DANGLE)indicates how a read transaction is required to progress through a system
input  logic [          3-1:0] arprot,    //protection attributes of a read transaction
input  logic [          4-1:0] arqos,     //(DANGLE)quality of Service identifier for a read transaction
input  logic [          4-1:0] arregion,  //(DANGLE)region indicator for a read transaction
input  logic [WIDTH_RUSER-1:0] aruser,    //user-defined extension for the read address channel
input  logic                   arvalid,   //indicates that the read address channel signals are valid
output logic                   arready,   //indicates that a transfer on the read address channel can be accepted

// R channel
output logic [WIDTH_RID  -1:0] rid,       //identification tag for a read reponse
output logic [WIDTH_DATA -1:0] rdata,     //read data
output logic [          2-1:0] rresp,     //read response, indicates the status of a read transaction
output logic                   rlast,     //indicates whether this is the last data transfer in a read transaction
output logic [WIDTH_RUSER-1:0] ruser,     //user-defined extension for the read data channel
output logic                   rvalid,    //indicates that the read data channel signals are valid
input  logic                   rready,    //indicates that a transfer on the read data channel can be accepted

//system
input  logic                   clk,       //operation clock
input  logic                   rst_n      //asynchronous active-low reset
); //axi4SubordinateModel
`protect128

////////////////////////////////////////////////////////////////////////////////
//Signal Declaration
/*{{{*/
typedef enum bit[1:0] {FIXED     = 2'b00, //fixed burst, no addr increase for beats
                       INCR      = 2'b01, //addr increases with beats
                       WRAP      = 2'b10, //addr increases but wrap around at a boundary
                       RESERVED  = 2'b11}
                       t_AXI_BURST;

typedef enum bit      {NORMAL    = 1'b0,  //normal access
                       EXCLUSIVE = 1'b1}  //exclusive access
                       t_AXI_LOCK;

typedef enum bit[1:0] {OKAY      = 2'b00, //normal access okay
                       EXOKAY    = 2'b01, //exclusive access okay
                       SLVERR    = 2'b10, //slave error
                       DECERR    = 2'b11} //decode error
                       t_AXI_RESP;

typedef struct{
  logic [WIDTH_WID   -1:0] id;          //transaction id
  logic [WIDTH_ADDR  -1:0] addr;        //byte address to access
  int                      len;         //number of beat
  int                      size;        //Bytes size of each beat
  int                      burst;       //burst type
  bit                      lock;        //atomic characteristics
  int                      protect;     //access permission control
  logic [WIDTH_WUSER -1:0] user;        //user-define extension
  logic [WIDTH_DATA  -1:0] data[$];     //write data
  logic [WIDTH_STRB  -1:0] strb[$];     //byte strobe

  logic [WIDTH_ADDR  -1:0] addrEnd;     //end of byte address to access
  logic [WIDTH_ADDR  -1:0] addrInc;     //address accumulate size *beat
} t_recordAW; //type of record to capture AW channel

typedef struct{
  logic [WIDTH_WID  -1:0] id;           //transaction id
  logic [WIDTH_ADDR -1:0] addr;         //byte address to access
  int                     len;          //number of beat
  int                     size;         //Bytes size of each beat
  int                     burst;        //burst type
  bit                     lock;         //atomic characteristics
  int                     protect;      //access permission control
  logic [WIDTH_RUSER-1:0] user;         //user-define extension
} t_recordAR; //type of record to capture AR channel

typedef struct{
  logic [WIDTH_WID  -1:0] id;           //transaction id
  logic [          2-1:0] resp;         //write reponse
  logic [WIDTH_WUSER-1:0] user;         //user-define extension
} t_recordB;  //type of record for B channel

typedef struct{
  logic [WIDTH_RID  -1:0] id;           //transaction id
  logic [WIDTH_DATA -1:0] data;         //read data
  logic [          2-1:0] resp;         //read reponse
  logic                   last;         //last beat indicator
  logic [WIDTH_RUSER-1:0] user;         //user-define extension
} t_recordR;  //type of record for R channel

//AMBA signal to drive
logic                   nxtAwready;     //indicates that a transfer on the write address channel can be accepted
logic                   nxtWready;      //indicates that a transfer on the write data channel can be accepted
logic [WIDTH_WID  -1:0] nxtBid;         //identification tag for a write reponse
logic [          2-1:0] nxtBresp;       //write response, indicates the status of a write transaction
logic [WIDTH_WUSER-1:0] nxtBuser;       //user-defined extension for the write reponse channel
logic                   nxtBvalid;      //indicates that the write response channel signals are valid
logic                   nxtArready;     //indicates that a transfer on the read address channel can be accepted
logic [WIDTH_RID  -1:0] nxtRid;         //identification tag for a read reponse
logic [WIDTH_DATA -1:0] nxtRdata;       //read data
logic [          2-1:0] nxtRresp;       //read response, indicates the status of a read transaction
logic                   nxtRlast;       //indicates whether this is the last data transfer in a read transaction
logic [WIDTH_RUSER-1:0] nxtRuser;       //user-defined extension for the read data channel
logic                   nxtRvalid;      //indicates that the read data channel signals are valid

//outstanding
int cntFlightWr = 0;    //number of outstanding transactions for write
int cntFlightRd = 0;    //number of outstanding transactions for read

int dueWrData   = 0;    //number of write data waiting

t_recordAW  qAW[$];     //queue keeping AW transfer
t_recordAR  qAR[$];     //queue keeping AR transfer

t_recordAW  qW[$];      //queue to handle W channel

t_recordB   qB[$];      //queue to handle B channel
t_recordR   qR[$];      //queue to handle R channel
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Instantiation
/*{{{*/
busSubordinateModel #(
.WIDTH_DATA      (WIDTH_DATA),        //bit width of data
.WIDTH_STRB      (WIDTH_STRB),        //bit width of data mask (MNEMONIC)

.TYPE_ARB_WR     (TYPE_ARB_WR),       //arbitration type for write ID
.TYPE_ARB_RD     (TYPE_ARB_RD),       //arbitration type for  read ID

.TYPE_LATENCY_WR (TYPE_LATENCY_WR),   //latency model for write
.LAT_WR_MAX_DEV  (LAT_WR_MAX_DEV),    //latency model param for write
.LAT_WR_MIN_AVG  (LAT_WR_MIN_AVG),    //latency model param for write

.TYPE_LATENCY_RD (TYPE_LATENCY_RD),   //latency model for read
.LAT_RD_MAX_DEV  (LAT_RD_MAX_DEV),    //latency model param for read
.LAT_RD_MIN_AVG  (LAT_RD_MIN_AVG),    //latency model param for read

.MAX_MESSAGE     (MAX_MESSAGE),       //max num of warning/error messages
.EN_WAR_CHECK    (EN_WAR_CHECK)       //0: disable read-prior-to-write check (Write-After-Read)
)
u_subordinate (
.clk    (clk),    //operation clock
.rst_n  (rst_n)   //asynchronous active-low reset
); //u_subordinate
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Procedures
/*{{{*/
initial begin: param_report/*{{{*/
//report parameters
`define STR(str) `"str`"    //stringify
`define PARAM_REPORT_HEX(X,N) $display("[NOTE] @%m %s: 0x%s", `STR(X), $psprintf({"%0",$psprintf("%0d",(N)),"x"}, (X)))
`define PARAM_REPORT_INT(X)   $display("[NOTE] @%m %s: %0d",  `STR(X), (X))
`define PARAM_REPORT_STR(X)   $display("[NOTE] @%m %s:'%s'",  `STR(X), (X))

`PARAM_REPORT_INT(WIDTH_WID);         //bit width of AW & B channel ID
`PARAM_REPORT_INT(WIDTH_RID);         //bit width of AR & R channel ID
`PARAM_REPORT_INT(WIDTH_ADDR);        //bit width of address

`PARAM_REPORT_INT(WIDTH_WUSER);       //bit width of awuser & buser
`PARAM_REPORT_INT(WIDTH_RUSER);       //bit width of aruser & ruser

`PARAM_REPORT_INT(REGION_BASE);       //starting address allocated for this module
`PARAM_REPORT_INT(REGION_SIZE);       //size of address region allocated for this module

`PARAM_REPORT_INT(OUTSTAND_WR);       //max num of outstandings for write
`PARAM_REPORT_INT(OUTSTAND_RD);       //max num of outstandings for read

`undef PARAM_REPORT_STR
`undef PARAM_REPORT_INT
`undef PARAM_REPORT_HEX
`undef STR
end //param_report/*}}}*/

//register write reponse
task automatic rxWr(int txId);/*{{{*/
  t_recordAW txW;     //a write transaction
  t_recordB  pkt;     //a write reponse packet

  bit        match;   //a matched ID found
  int        numId;   //number of IDs waiting write response

  match = 1'b0;
  numId = qW.size();

  //pick oldest transaction for txId
  for(int i = 0; i < numId; i++) begin/*{{{*/
    if(qW[i].id == txId) begin
      match = 1'b1;
      txW   = qW[i];

      qW.delete(i);   //NOTE; out-of-order completion is possible
    end //if
  end //for i/*}}}*/

  UNKNOWN_BID:
  assert(match) else $fatal("[%m][FATAL] unmatched write ID(%0d) detected", txId);

  //prepare a outgoing beat
  pkt.id   = txW.id;
  pkt.resp = OKAY;
  pkt.user = txW.user;

  qB.push_back(pkt);  //register a beat

endtask //rxWr/*}}}*/

//register read reponse
task automatic rxRd(int txId, logic [WIDTH_DATA-1:0] data[$]);/*{{{*/
  t_recordAR txR;     //a read transaction
  t_recordR  pkt;     //a read data beat

  bit        match;   //a matched ID found
  int        len;     //number of incoming words
  int        amtTx;   //total amount of TX in bits
  int        stride;  //size of each outgoing best in terms of bits
  int        cntTx;   //counting outgoing beats for a transaction

  match = 1'b0;

  //pick oldest transaction for txId
  for(int i = 0; i < qAR.size(); i++) begin/*{{{*/
    if(qAR[i].id == txId) begin
      match = 1'b1;
      txR   = qAR[i];

      qAR.delete(i);  //NOTE: out-of-order completion is possible
    end //if
  end //for i/*}}}*/

  UNKNOWN_RID:
  assert(match) else $fatal("[%m][FATAL] unmatched read ID(%0d) detected", txId);

  //prepare outgoing beat stream
  len       = data.size();
  stride    = 8 *(2 **txR.size); //byte size of a beat = 2**size
  amtTx     = stride *(txR.len +1);

  cntTx     = 0;

  //stride should <= WIDTH_DATA
  if(stride > WIDTH_DATA) stride = WIDTH_DATA;

  //coin a outgoing beat
  pkt.id    = txR.id;
  pkt.resp  = OKAY;
  pkt.user  = txR.user;

  //feed read data queue considering length and size
  for(int i = 0; i < len; i++) begin/*{{{*/
    logic [WIDTH_DATA-1:0] wBuf = data.pop_front();

    //NOTE: wide transfer is already considered during address conversion
    //      between Byte and word. Howerver, to handle a burst,
    //      both of wide & narrow transfer should be considered here
    //      since burst size still have a dependency on beat size
    for(int j = 0; j < WIDTH_DATA; j += stride) begin
      pkt.data = wBuf;   //allowing duplicated for narrow transfer

      cntTx++;

      pkt.last = ((i == len -1)               //last word
               && (stride *cntTx >= amtTx));  //last beat

      qR.push_back(pkt);  //register a beat

      if(pkt.last) break;
    end //for j

  end //for i/*}}}*/

  UNMATCHED_BURST_LENGTH:
  assert(pkt.last && (stride *cntTx) == amtTx)
  else $fatal("[%m][FATAL] unmatched burst length for ID(%0d) detected: expected %0d, observed %0d",
                           txR.id, amtTx /stride, cntTx);
endtask //rxRd/*}}}*/

//delete all pending transaction whenever rst_n is asserted
always @(negedge rst_n) begin: p_reset_init/*{{{*/
  qAW.delete();       qW.delete();      qB.delete();
  qAR.delete();       qR.delete();

  nxtAwready  = 0;    nxtWready = 0;
  nxtWready   = 0;

  cntFlightWr = 0;    dueWrData = 0;
  cntFlightRd = 0;
end //p_reset_init/*}}}*/

//capture AW channel
task automatic doAW();/*{{{*/
  t_recordAW pkt;       //packet for AW request
  longint    bAddrEnd;  //end address of the request in bytes

  pkt.id      = awid;       pkt.len     = awlen;
  pkt.addr    = awaddr;     pkt.size    = awsize;
                            pkt.burst   = awburst;

  pkt.lock    = awlock;   //TODO: implment this feature
  pkt.protect = awprot;   //TODO: implment this feature
  pkt.user    = awuser;   //TODO: implment this feature

  bAddrEnd    = awaddr +((2 **awsize) *(awlen +1)) -1;

  pkt.data.delete();
  pkt.strb.delete();

  pkt.addrEnd = bAddrEnd;
  pkt.addrInc = awaddr;

  //check region address range
  CHECK_OUT_OF_BOUNDARY:
  assert((awaddr >= REGION_BASE) && (bAddrEnd < (REGION_BASE +REGION_SIZE)))
//else $fatal("[%m][FATAL] out-of-boundary access detected awaddr: 0x%0x, awlen: 0x%02x, awsize: 0x%01x, awburst: 0x%01x",
//                                                         awaddr, awlen, awsize, awburst);
  else $fatal("[%m][FATAL] out-of-boundary access detected awaddr: 0x%0x, awlen: 0x%02x, awsize: 0x%01x, awburst: 0x%01x, bAddrEnd: 0x%01x, REGION_BASE: 0x%01x, REGION_SIZE: 0x%01x, ",
                                                           awaddr, awlen, awsize, awburst, bAddrEnd, REGION_BASE, REGION_SIZE);
  //check 4K boundary, 4K = 2**12
  CHECK_4K_BOUNDARY_CROSSING:
  assert((awaddr >> 12) == (bAddrEnd >> 12))
  else $fatal("[%m][FATAL] 4K boundary violation detected awaddr: 0x%0x, awlen: 0x%02x, awsize: 0x%01x, awburst: 0x%01x",
                                                          awaddr, awlen, awsize, awburst);

  qAW.push_back(pkt); //register a write request

endtask //doAW/*}}}*/

//cpature W channel
task automatic doW();/*{{{*/
  t_recordAW             pkt;     //packet in qAW

  logic [WIDTH_DATA-1:0] wObtain; //word fetch  buffer
  logic [WIDTH_STRB-1:0] wStrobe; //word strobe buffer

  logic [WIDTH_DATA-1:0] wResDat; //word result data
  logic [WIDTH_STRB-1:0] wResStb; //word result strobe

  int                    stride;  //byte stride from awsize

  assert(qAW.size())
  else $fatal("[%m][FATAL] write data is accepted before write address is accepted");

  //calculate stride for the next addrInc
  stride = 2 **(qAW[0].size);

  if(stride *8 >= WIDTH_DATA) begin       //NOT a narrow transfer
    qAW[0].data.push_back(wdata);
    qAW[0].strb.push_back(wstrb);
  end //if
  else begin                              //narrow transafer case
    //prepare a buffer for data & strobe
    if(0 == (qAW[0].addrInc %WIDTH_STRB)) begin  //new word
      wObtain = 'b0;
      wStrobe = 'b0;
    end //if
    else begin                            //fetch the last pair from the queue
      wObtain = qAW[0].data.pop_back();
      wStrobe = qAW[0].strb.pop_back();
    end //else

    //fill a word based on the strobes
    for(int i = 0; i < WIDTH_STRB; i++) begin
      //NOTE: for narrow transfer wstrb should contain that information
           if(wstrb  [i]) begin wResDat[8*i+:8] = wdata  [8*i+:8];  wResStb[i] = wstrb  [i]; end
      else if(wStrobe[i]) begin wResDat[8*i+:8] = wObtain[8*i+:8];  wResStb[i] = wStrobe[i]; end
      else                begin wResDat[8*i+:8] = 8'b0;             wResStb[i] = 1'b0;       end
    end //for i

    //write back
    qAW[0].data.push_back(wResDat);
    qAW[0].strb.push_back(wResStb);
  end //else

  qAW[0].addrInc += stride;   //set the next address

  if(wlast) begin
    pkt = qAW.pop_front();    //move one from qAW to qW
    qW.push_back(pkt);

    CHECK_WLAST:
    assert(pkt.addrInc == (pkt.addrEnd +1))
    else $fatal("[%m][FATAL] wlast comes in a wrong timing");

    //call busSubordinateModel
    u_subordinate.txWr(
      .txId       (pkt.id),
      .bAddrStart (pkt.addr),
      .bAddrEnd   (pkt.addrEnd),
      .data       (pkt.data),
      .strb       (pkt.strb));
  end //if
endtask //doW/*}}}*/

//handle B channel
task automatic doB();/*{{{*/
  t_recordB pktB;

  pktB      = qB.pop_front();  //pop a beat

  nxtBid    = pktB.id;
  nxtBresp  = pktB.resp;
  nxtBuser  = pktB.user;
endtask //doB/*}}}*/

//backdoor write
function automatic backdoorWr(/*{{{*/
  longint        bAddr,   //Byte address to access
  const ref byte data     //write data
);

  //NOTE: address checking is bus protocol specific
  CHECK_OUT_OF_BOUNDARY:
  assert((bAddr >= REGION_BASE) && (bAddr < (REGION_BASE +REGION_SIZE)))
  else $fatal("[%m][FATAL] out-of-boundary access detected addr: 0x%0x", bAddr);

  u_subordinate.backdoorWr(bAddr, data);    //carry-over

endfunction //backdoorWr/*}}}*/

//backdoor read
function automatic byte backdoorRd(/*{{{*/
  longint        bAddr     //Byte address to access
);

  //NOTE: address checking is bus protocol specific
  CHECK_OUT_OF_BOUNDARY:
  assert((bAddr >= REGION_BASE) && (bAddr < (REGION_BASE +REGION_SIZE)))
  else $fatal("[%m][FATAL] out-of-boundary access detected addr: 0x%0x", bAddr);

  return u_subordinate.backdoorRd(bAddr);   //carry-over

endfunction //backdoorRd/*}}}*/

//hanlde write transaction
initial begin: p_write/*{{{*/
  forever begin
    @(negedge clk iff rst_n);

    //handle B channel
    if(!bvalid                  //start condition
    || (bvalid && bready))      //a beat is transfered
    begin
      if(qB.size()) begin       //some reponse is ready
        doB();

        nxtBvalid = 1'b1;

        cntFlightWr--;          //end of a write transaction
      end //if
      else nxtBvalid = 1'b0;
    end //if

    //handle W channel
    if(wvalid & wready) begin
      doW();                    //capture W channel

      dueWrData--;
    end //if

    //handle AW channel
    if(awvalid & awready) begin
      doAW();                   //capture AW channel

      cntFlightWr++;            //start of a write transaction
      dueWrData += awlen +1;
    end //if

    nxtAwready = (cntFlightWr < OUTSTAND_WR);
    nxtWready  = (dueWrData   > 0);
  end //forever
end //p_write/*}}}*/

//capture AR channel
task automatic doAR();/*{{{*/
  t_recordAR pkt;       //packet for AR request
  longint    bAddrEnd;  //end address of the request in bytes

  pkt.id      = arid;       pkt.len     = arlen;
  pkt.addr    = araddr;     pkt.size    = arsize;
                            pkt.burst   = arburst;

  pkt.lock    = arlock;   //TODO: implment this feature
  pkt.protect = arprot;   //TODO: implment this feature
  pkt.user    = aruser;   //TODO: implment this feature

  bAddrEnd    = araddr +((2 **arsize) *(arlen +1)) -1;

  //check region address range
  CHECK_OUT_OF_BOUNDARY:
  assert((araddr >= REGION_BASE) && (bAddrEnd < (REGION_BASE +REGION_SIZE)))
  else $fatal("[%m][FATAL] out-of-boundary access detected araddr: 0x%0x, arlen: 0x%02x, arsize: 0x%01x, arburst: 0x%01x",
                                                           araddr, arlen, arsize, arburst);

  //check 4K boundary, 4K = 2**12
  CHECK_4K_BOUNDARY_CROSSING:
  assert((araddr >> 12) == (bAddrEnd >> 12))
  else $fatal("[%m][FATAL] 4K boundary violation detected araddr: 0x%0x, arlen: 0x%02x, arsize: 0x%01x, arburst: 0x%01x",
                                                          araddr, arlen, arsize, arburst);

  qAR.push_back(pkt); //register

  //call busSubordinateModel
  u_subordinate.txRd(
    .txId       (arid),
    .bAddrStart (araddr),
    .bAddrEnd   (bAddrEnd));

endtask //doAR/*}}}*/

//handle read transaction
initial begin: p_read/*{{{*/
  t_recordR pkt;

  forever begin
    @(negedge clk iff rst_n);

    //handle R channel
    if(!rvalid                  //start condition
    || (rvalid && rready))      //a beat is transfered
    begin
      if(qR.size()) begin       //some rdata is ready
        pkt = qR.pop_front();   //pop a beat

          {nxtRid, nxtRdata, nxtRresp, nxtRlast, nxtRuser}
        = {pkt.id, pkt.data, pkt.resp, pkt.last, pkt.user};

        nxtRvalid = 1'b1;

        if(pkt.last) cntFlightRd--;  //end of a read transaction
      end //if
      else nxtRvalid = 1'b0;
    end //if

    //handle AR channel
    if(arvalid & arready) begin
      doAR();         //capture AR channel

      cntFlightRd++;  //start of a read transaction
    end //if

    nxtArready = (cntFlightRd < OUTSTAND_RD);
  end //forever
end //p_read/*}}}*/

//drive AMBA signals
always @(posedge clk, negedge rst_n) begin: p_amba/*{{{*/
  if(!rst_n) begin
    awready   <= '0;
     wready   <= '0;
    arready   <= '0;

   {bid, bresp, bvalid}                  <= '0;
   {rid, rdata, rresp, rlast, rvalid}    <= '0;

   {nxtBid, nxtBresp, nxtBvalid}                      <= '0;
   {nxtRid, nxtRdata, nxtRresp, nxtRlast, nxtRvalid}  <= '0;

    buser     <= '0;          nxtBuser   <= '0;
    ruser     <= '0;          nxtRuser   <= '0;
  end //if
  else begin
    awready   <= nxtAwready;
     wready   <= nxtWready;
    arready   <= nxtArready;

   {bid, bresp, bvalid}               <= {nxtBid, nxtBresp, nxtBvalid};
   {rid, rdata, rresp, rlast, rvalid} <= {nxtRid, nxtRdata, nxtRresp, nxtRlast, nxtRvalid};

    buser     <= nxtBuser;
    ruser     <= nxtRuser;
  end //else
end //p_amba/*}}}*/

/*}}}*/

endmodule //axi4SubordinateModel

////////////////////////////////////////////////////////////////////////////////
//Built-in Test Bench
/*{{{*/
`ifndef SYNTHESIS
`ifdef __EN_BUILT_IN_TEST_BENCH__

//synopsys translate_off
/*{{{*/
module tb_axi4SubordinateModel();

`define  __TESTBENCH_NAME__  tb_axi4SubordinateModel
localparam TESTBENCH_NAME = "tb_axi4SubordinateModel";

////////////////////////////////////////////////////////////////////////////////
//Parameter List
/*{{{*/
//testbench paramter
parameter TEST_CFG_NAME = "BUILT_IN_TEST";  //test configuration name

//DUT parameter
//AMBA
parameter  WIDTH_WID       = 4;             //bit width of AW & B channel ID
parameter  WIDTH_RID       = 4;             //bit width of AR & R channel ID
parameter  WIDTH_ADDR      = 32;            //bit width of address
parameter  WIDTH_DATA      = 128;           //bit width of data
localparam WIDTH_STRB      = WIDTH_DATA/8;  //bit width of data mask (MNEMONIC)
localparam WIDTH_BYTE      = WIDTH_DATA/8;  //bit width of data in Bytes (MNEMONIC)

parameter   WIDTH_WUSER    = 1;             //bit width of awuser & buser
parameter   WIDTH_RUSER    = 1;             //bit width of aruser & ruser

//system topology
parameter  REGION_BASE     = 'h0;           //starting address allocated for this module
parameter  REGION_SIZE     = 'h100000;      //size of address region allocated for this module

//throughput
parameter  OUTSTAND_WR     = 16;            //max num of outstandings for write
parameter  OUTSTAND_RD     = 16;            //max num of outstandings for read

//arbitration
parameter  TYPE_ARB_WR     = "FCFS";        //arbitration type for write ID
                                            //  "RAND" or "RR" or "FCFS"
parameter  TYPE_ARB_RD     = "FCFS";        //arbitration type for  read ID
                                            //  "RAND" or "RR" or "FCFS"

//latency
parameter  TYPE_LATENCY_WR = "UNIFORM";     //latency model for write
                                            //  "UNIFORM"; "ERLANG", "GAUSSIAN"
                                            //  or "POISSON";
parameter  LAT_WR_MAX_DEV  = 4;             //latency model param for write
                                            //  UNIFORM : max
                                            //  ERLANG  : k stage
                                            //  GAUSSIAN: dev
                                            //  POISSON : NO MEANING
parameter  LAT_WR_MIN_AVG  = 4;             //latency model param for write
                                            //  UNIFORM : min
                                            //  ERLANG  : mean
                                            //  GAUSSIAN: mean
                                            //  POISSON : mean

parameter  TYPE_LATENCY_RD = "UNIFORM";     //latency model for read
                                            //  "UNIFORM"; "ERLANG", "GAUSSIAN"
                                            //  or "POISSON";
parameter  LAT_RD_MAX_DEV  = 4;             //latency model param for read
                                            //  UNIFORM : max
                                            //  ERLANG  : k stage
                                            //  GAUSSIAN: dev
                                            //  POISSON : NO MEANING
parameter  LAT_RD_MIN_AVG  = 4;             //latency model param for read
                                            //  UNIFORM : min
                                            //  ERLANG  : mean
                                            //  GAUSSIAN: mean
                                            //  POISSON : mean

//logging
parameter  MAX_MESSAGE     = 1000;          //max num of warning/error messages
parameter  EN_WAR_CHECK    = 1;             //0: disable read-prior-to-write check (Write-After-Read)

///////////////////////////////////////////////////////////////////////////////
//Parameter report
initial begin: param_report/*{{{*/
//NOTE: all testbench paramters MUST be reported HERE
//NOTE: all DUT parameters will be reported by the DUT

`define STR(str) `"str`"    //stringify
`define PARAM_REPORT_HEX(X,N) $display("[NOTE] @%m %s: 0x%s", `STR(X), $psprintf({"%0",$psprintf("%0d",(N)),"x"}, (X)))
`define PARAM_REPORT_INT(X)   $display("[NOTE] @%m %s: %0d",  `STR(X), (X))
`define PARAM_REPORT_STR(X)   $display("[NOTE] @%m %s:'%s'",  `STR(X), (X))

`PARAM_REPORT_STR(TEST_CFG_NAME);     //test configuration name

`undef PARAM_REPORT_STR
`undef PARAM_REPORT_INT
`undef PARAM_REPORT_HEX
`undef STR
end //param_report/*}}}*/
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Clock and Reset
/*{{{*/
localparam HALF_PERIOD = 5;   //half preiod of clk in ns

bit oscillator;     //self-running oscillator

bit clk;            //operation clock
bit rst_n;          //active-low reset

longint cntClk = 0; //cycle counter

initial forever #(HALF_PERIOD*1ns) oscillator = ~oscillator;

always_ff @(posedge oscillator) clk    <= ~clk;
always_ff @(posedge clk)        cntClk <=  cntClk +1;

initial begin: p_rst
  repeat (10) @(posedge clk);

  @(posedge clk) rst_n <= 1'b1;
end //p_rst
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Waveform dump: Full dump assuming small DUT
/*{{{*/
`ifndef VCS
`ifndef NCSIM
`define VCS   //default simulator
`endif//NCSIM
`endif//VCS

`ifdef NCSIM/*{{{*/
initial begin: proc_dump_shm
    $display ("[NOTE] @%m Open shm '%s.shm'", TESTBENCH_NAME);
    $shm_open({TESTBENCH_NAME, ".shm"},    //db_name
                ,       //is_sequence_time; dump all value change in a cycle
                ,       //db_size; maximum size in Bytes
                1,      //is_compression
                1024,   //incsize; incremental file size in MBytes
                200);   //incfiles; number of incremental files

    //NOTE: probe node specifier
    //  'A' : all nodes
    //  'S' : all nodes in all instantiation below, except library cells
    //  'C' : all nodes in all instantiation below, including library cells
    //  'M' : memory
    //  'T' : task
    //  'F' : function

    $shm_probe(`__TESTBENCH_NAME__, "AMCTF");
end //proc_dump_shm
`endif//NCSIM/*}}}*/

`ifdef VCS/*{{{*/
initial begin: proc_dump_vpd
    $display ("[NOTE] @%m Open vpd '%s.vpd'", TESTBENCH_NAME);
    $vcdplusfile({TESTBENCH_NAME, ".vpd"});

    $vcdpluson;
end //proc_dump_vpd
`endif//VCS/*}}}*/
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Verdi
/*{{{*/
`ifdef  VERDI
initial begin: proc_dump_fsdb
    int chunkSize = 1024;   //1GBytes

    $fsdbDumpfile({TESTBENCH_NAME, ".fsdb"});

    //slice FSDB into chunks
    $fsdbAutoSwitchDumpfile(chunkSize, {TESTBENCH_NAME, ".fsdb"}, 0, {TESTBENCH_NAME, ".log"});

    $fsdbDumpvars(0, TESTBENCH_NAME);

    $display ("[NOTE] @%m Open FSDB '%s.fsdb'", TESTBENCH_NAME);
end //proc_dump_fsdb
`endif//VERDI
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Signal Declaration
/*{{{*/

typedef struct{
  int id;     //ID
  int addr;   //address to access
  int size;   //size of each beat in Bytes
  int len;    //number of data beats
} t_tx;  //access

//AW channel
logic [WIDTH_WID  -1:0] awid;       //identification tag for a write transaction
logic [WIDTH_ADDR -1:0] awaddr;     //address of the first transfer in a write transaction
logic [          8-1:0] awlen;      //exact number of data transfers in a write transaction
logic [          3-1:0] awsize;     //num of Bytes in each data transfer in a write transaction
logic [          2-1:0] awburst;    //burst type
logic                   awlock;     //atomic characteristics of a write transaction
logic [          4-1:0] awcache;    //(DANGLE)indicates how a write transaction is required to progress through a system
logic [          3-1:0] awprot;     //protection attributes of a write transaction
logic [          4-1:0] awqos;      //(DANGLE)quality of Service identifier for a write transaction
logic [          4-1:0] awregion;   //(DANGLE)region indicator for a write transaction
logic [WIDTH_WUSER-1:0] awuser;     //user-defined extension for the write address channel
logic                   awvalid;    //indicates that the write address channel signals are valid
logic                   awready;    //indicates that a transfer on the write address channel can be accepted

// W channel
logic [WIDTH_DATA -1:0] wdata;      //write data
logic [WIDTH_STRB -1:0] wstrb;      //write strobe, indicate which Byte lanes hold valid data
logic                   wlast;      //indicates whether this is the last data transfer in a write transaction
logic                   wvalid;     //indicates that the write data channel signals are valid
logic                   wready;     //indicates that a transfer on the write data channel can be accepted

// B channel
logic [WIDTH_WID  -1:0] bid;        //identification tag for a write reponse
logic [          2-1:0] bresp;      //write response, indicates the status of a write transaction
logic [WIDTH_WUSER-1:0] buser;      //user-defined extension for the write reponse channel
logic                   bvalid;     //indicates that the write response channel signals are valid
logic                   bready;     //indicates that a transfer on the write reponse channel can be accepted

//AR channel
logic [WIDTH_RID  -1:0] arid;       //identification tag for a read transaction
logic [WIDTH_ADDR -1:0] araddr;     //address of the first transfer in a read transaction
logic [          8-1:0] arlen;      //exact number of data transfers in a read transaction
logic [          3-1:0] arsize;     //num of Bytes in each data transfer in a read transaction
logic [          2-1:0] arburst;    //burst type
logic                   arlock;     //atomic characteristics of a read transaction
logic [          4-1:0] arcache;    //(DANGLE)indicates how a read transaction is required to progress through a system
logic [          3-1:0] arprot;     //protection attributes of a read transaction
logic [          4-1:0] arqos;      //(DANGLE)quality of Service identifier for a read transaction
logic [          4-1:0] arregion;   //(DANGLE)region indicator for a read transaction
logic [WIDTH_RUSER-1:0] aruser;     //user-defined extension for the read address channel
logic                   arvalid;    //indicates that the read address channel signals are valid
logic                   arready;    //indicates that a transfer on the read address channel can be accepted

// R channel
logic [WIDTH_RID  -1:0] rid;        //identification tag for a read reponse
logic [WIDTH_DATA -1:0] rdata;      //read data
logic [          2-1:0] rresp;      //read response, indicates the status of a read transaction
logic                   rlast;      //indicates whether this is the last data transfer in a read transaction
logic [WIDTH_RUSER-1:0] ruser;      //user-defined extension for the read data channel
logic                   rvalid;     //indicates that the read data channel signals are valid
logic                   rready;     //indicates that a transfer on the read data channel can be accepted

//visualize event in each channel
event tick_AW;    //write address
event tick_W;     //write data
event tick_B;     //write reponse
event tick_AR;    //read  address
event tick_R;     //read  data
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Instantiation
/*{{{*/
axi4SubordinateModel #(/*{{{*/
//AMBA
.WIDTH_WID       (WIDTH_WID),             //bit width of AW & B channel ID
.WIDTH_RID       (WIDTH_RID),             //bit width of AR & R channel ID
.WIDTH_ADDR      (WIDTH_ADDR),            //bit width of address
.WIDTH_DATA      (WIDTH_DATA),            //bit width of data

.WIDTH_WUSER     (WIDTH_WUSER),           //bit width of awuser & buser
.WIDTH_RUSER     (WIDTH_RUSER),           //bit width of aruser & ruser

//system topology
.REGION_BASE     (REGION_BASE),           //starting address allocated for this module
.REGION_SIZE     (REGION_SIZE),           //size of address region allocated for this module

//throughput
.OUTSTAND_WR     (OUTSTAND_WR),           //max num of outstandings for write
.OUTSTAND_RD     (OUTSTAND_RD),           //max num of outstandings for read

//arbitration
.TYPE_ARB_WR     (TYPE_ARB_WR),           //arbitration type for write ID
                                          //  "FCFS", "RAND" or "RR"
.TYPE_ARB_RD     (TYPE_ARB_RD),           //arbitration type for  read ID
                                          //  "FCFS", "RAND" or "RR"

//latency
.TYPE_LATENCY_WR (TYPE_LATENCY_WR),       //latency model for write
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
.LAT_WR_MAX_DEV  (LAT_WR_MAX_DEV),            //latency model param for write
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
.LAT_WR_MIN_AVG  (LAT_WR_MIN_AVG),             //latency model param for write
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

.TYPE_LATENCY_RD (TYPE_LATENCY_RD),       //latency model for read
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
.LAT_RD_MAX_DEV  (LAT_RD_MAX_DEV),        //latency model param for read
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
.LAT_RD_MIN_AVG  (LAT_RD_MIN_AVG),        //latency model param for read
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

//logging
.MAX_MESSAGE     (MAX_MESSAGE),           //max num of warning/error messages
.EN_WAR_CHECK    (EN_WAR_CHECK)           //0: disable read-prior-to-write check (Write-After-Read)
)/*}}}*/
dut(/*{{{*/
//AW channel
.awid       (awid),       //identification tag for a write transaction
.awaddr     (awaddr),     //address of the first transfer in a write transaction
.awlen      (awlen),      //exact number of data transfers in a write transaction
.awsize     (awsize),     //num of Bytes in each data transfer in a write transaction
.awburst    (awburst),    //burst type
.awlock     (awlock),     //atomic characteristics of a write transaction
.awcache    (awcache),    //(DANGLE)indicates how a write transaction is required to progress through a system
.awprot     (awprot),     //protection attributes of a write transaction
.awqos      (awqos),      //(DANGLE)quality of Service identifier for a write transaction
.awregion   (awregion),   //(DANGLE)region indicator for a write transaction
.awuser     (awuser),     //user-defined extension for the write address channel
.awvalid    (awvalid),    //indicates that the write address channel signals are valid
.awready    (awready),    //indicates that a transfer on the write address channel can be accepted

// W channel
.wdata      (wdata),      //write data
.wstrb      (wstrb),      //write strobe, indicate which Byte lanes hold valid data
.wlast      (wlast),      //indicates whether this is the last data transfer in a write transaction
.wvalid     (wvalid),     //indicates that the write data channel signals are valid
.wready     (wready),     //indicates that a transfer on the write data channel can be accepted

// B channel
.bid        (bid),        //identification tag for a write reponse
.bresp      (bresp),      //write response, indicates the status of a write transaction
.buser      (buser),      //user-defined extension for the write reponse channel
.bvalid     (bvalid),     //indicates that the write response channel signals are valid
.bready     (bready),     //indicates that a transfer on the write reponse channel can be accepted

//AR channel
.arid       (arid),       //identification tag for a read transaction
.araddr     (araddr),     //address of the first transfer in a read transaction
.arlen      (arlen),      //exact number of data transfers in a read transaction
.arsize     (arsize),     //num of Bytes in each data transfer in a read transaction
.arburst    (arburst),    //burst type
.arlock     (arlock),     //atomic characteristics of a read transaction
.arcache    (arcache),    //(DANGLE)indicates how a read transaction is required to progress through a system
.arprot     (arprot),     //protection attributes of a read transaction
.arqos      (arqos),      //(DANGLE)quality of Service identifier for a read transaction
.arregion   (arregion),   //(DANGLE)region indicator for a read transaction
.aruser     (aruser),     //user-defined extension for the read address channel
.arvalid    (arvalid),    //indicates that the read address channel signals are valid
.arready    (arready),    //indicates that a transfer on the read address channel can be accepted

// R channel
.rid        (rid),        //identification tag for a read reponse
.rdata      (rdata),      //read data
.rresp      (rresp),      //read response, indicates the status of a read transaction
.rlast      (rlast),      //indicates whether this is the last data transfer in a read transaction
.ruser      (ruser),      //user-defined extension for the read data channel
.rvalid     (rvalid),     //indicates that the read data channel signals are valid
.rready     (rready),     //indicates that a transfer on the read data channel can be accepted

//system
.clk        (clk),        //operation clock
.rst_n      (rst_n)       //asynchronous active-low reset
); //dut/*}}}*/
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Procedures
/*{{{*/
`define CASE(X) begin nameTest = (X); $display("[%m] ### test-%0d %s", cntTest++, (X)); end

task automatic idle(int n);/*{{{*/
  repeat(n) @(posedge clk);
endtask //idle/*}}}*/

//post a transfer in AW channel
task automatic postWrAddr(input t_tx pkt);/*{{{*/
  awvalid <= 1'b1;
     awid <= pkt.id;
   awaddr <= pkt.addr;
    awlen <= pkt.len -1;
   awsize <=$clog2(pkt.size);

  do @(posedge clk); while(!awready);

  -> tick_AW;
  awvalid <= 1'b0;
  $display("[%m] AW: id 0x%0x addr 0x%0x len %0d size %0d",
                        awid, awaddr, awlen, awsize);
endtask //postWrAddr/*}}}*/
//post a transfer in  W channel
task automatic postWrData(input t_tx pkt);/*{{{*/
  int idx = 1;

  wvalid <= 1'b1;
  wdata  <= pkt.addr;
  wstrb  <=(2**pkt.size -1) << (pkt.addr %WIDTH_STRB);
  wlast  <=(1 == pkt.len);

  repeat(pkt.len) begin
    do @(posedge clk);
    while (!wready);

    -> tick_W;
    wdata <= wdata +pkt.size;
    wlast <=(pkt.len == ++idx);
    wstrb <=(2**pkt.size -1) << ((wdata +pkt.size) %WIDTH_STRB);

    $display("[%m]  W: id 0x%0x addr 0x%0x data 0x%0x strb 0x%0x wlast %0d", pkt.id, wdata, wdata, wstrb, wlast);
  end //repeat

  wvalid <= 1'b0;

endtask //postWrData/*}}}*/
//wait a transfer in  B channel
task automatic waitWrResp(input t_tx pkt);/*{{{*/
  do @(posedge clk);
  while(!bvalid || !bready || (bid != pkt.id));

  -> tick_B;
  $display("[%m]  B: id 0x%0x", bid);
endtask //waitWrResp/*}}}*/

//post a transfer in AR channel
task automatic postRdAddr(input t_tx pkt);/*{{{*/
  arvalid <= 1'b1;
     arid <= pkt.id;
   araddr <= pkt.addr;
    arlen <= pkt.len -1;
   arsize <=$clog2(pkt.size);

  do @(posedge clk); while(!arready);

  -> tick_AR;
  arvalid <= 1'b0;
  $display("[%m] AR: id 0x%0x addr 0x%0x len %0d size %0d",
                        arid, araddr, arlen, arsize);
endtask //postRdAddr/*}}}*/
//wait a transfer in  R channel
task automatic waitRdData(input t_tx pkt);/*{{{*/
  int cnt;

  cnt = (pkt.size > WIDTH_STRB)
      ?  pkt.len *(pkt.size /WIDTH_STRB)
      :  pkt.len;

  repeat (cnt) begin
    do @(posedge clk);
    while(!rvalid || !rready || (rid != pkt.id));

    -> tick_R;
    $display("[%m]  R: id 0x%0x data 0x%0x rlast %0d", rid, rdata, rlast);
  end //repeat
endtask //waitRdData/*}}}*/

initial begin: p_stimulus
  string  nameTest;

  int cntTest = 0;    //test case number

  awvalid = 1'b0;
   wvalid = 1'b0;     bready = 1'b1;
  arvalid = 1'b0;     rready = 1'b1;

  awuser  = '0;
  aruser  = '0;

  awburst = 2'b01;    arburst = 2'b01;
  awlock  = '0;       arlock  = '0;
  awcache = '0;       arcache = '0;
  awprot  = '0;       arprot  = '0;

  @(posedge rst_n);

  idle(10);

  `CASE("Single Write");
  begin/*{{{*/
    t_tx pkt;

    pkt.id   = 1;
    pkt.addr = 'h100;
    pkt.size = WIDTH_STRB;
    pkt.len  = 1;

    fork
      postWrAddr(pkt);
      postWrData(pkt);
      waitWrResp(pkt);
    join

    idle(100);
  end /*}}}*/

  `CASE("Long Word Burst Write");
  begin/*{{{*/
    t_tx pkt;

    pkt.id   = 2;
    pkt.addr = 'h200;
    pkt.size = WIDTH_STRB;
    pkt.len  = 16;

    fork
      postWrAddr(pkt);
      postWrData(pkt);
      waitWrResp(pkt);
    join

    idle(100);
  end /*}}}*/

  `CASE("Long Byte Burst Write");
  begin/*{{{*/
    t_tx pkt;

    pkt.id   = 3;
    pkt.addr = 'h300;
    pkt.size = 1;
    pkt.len  = 32;

    fork
      postWrAddr(pkt);
      postWrData(pkt);
      waitWrResp(pkt);
    join

    idle(100);
  end /*}}}*/

  `CASE("Two consecutive Writes");
  begin/*{{{*/
    t_tx pkt1, pkt2;

    pkt1.id   = 4;            pkt2.id   = 5;
    pkt1.addr = 'h400;        pkt2.addr = 'h500;
    pkt1.size = 16;           pkt2.size = 16;
    pkt1.len  = 1;            pkt2.len  = 2;

    fork
      begin postWrAddr(pkt1); postWrAddr(pkt2); end
      begin postWrData(pkt1); postWrData(pkt2); end
      begin waitWrResp(pkt1); waitWrResp(pkt2); end
    join

    idle(100);
  end /*}}}*/

//  `CASE("Crossing-4K-boundary Write");
//  begin/*{{{*/
//    t_tx pkt;
//
//    pkt.id   = 6;
//    pkt.addr = 4*1024 -8;
//    pkt.size = 16;
//    pkt.len  = 1;
//
//    fork
//      postWrAddr(pkt);
//      postWrData(pkt);
//      waitWrResp(pkt);
//    join
//
//    idle(100);
//  end /*}}}*/

  `CASE("Single Read");
  begin/*{{{*/
    t_tx pkt;

    pkt.id   = 1;
    pkt.addr = 'h100;
    pkt.size = WIDTH_STRB;
    pkt.len  = 1;

    fork
      postRdAddr(pkt);
      waitRdData(pkt);
    join

    idle(100);
  end /*}}}*/

  `CASE("Two consecutive Reads");
  begin/*{{{*/
    t_tx pkt1, pkt2;

    pkt1.id   = 4;            pkt2.id   = 5;
    pkt1.addr = 'h400;        pkt2.addr = 'h500;
    pkt1.size = 4;            pkt2.size = 32;
    pkt1.len  = 3;            pkt2.len  = 1;

    fork
      begin postRdAddr(pkt1); postRdAddr(pkt2); end
      begin waitRdData(pkt1); waitRdData(pkt2); end
    join

    idle(100);
  end /*}}}*/

  `CASE("Backdoor Access");
  begin/*{{{*/
    longint addr = 'h200;

    byte data;             //   0      1      2      3      4      5      6      7
    byte expected[0:8-1] = {8'h00, 8'h02, 8'h00, 8'h00, 8'h00, 8'h00, 8'h00, 8'h00};

    for(int i=0; i < $size(expected); i++) begin
      data = dut.backdoorRd(addr +i);

      if(data != expected[i]) begin
        $display("[%m][ERROR] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x, while expecting 0x%0x",
                               cntClk, addr +i, data, expected[i]);
      end //if
      else begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x",
                               cntClk, addr +i, data);
      end //else
    end //for i

    begin
      int  idx[0:2-1] = {   3,    5};
      byte tmp[0:2-1] = {'hAB, 'hEF};
      int  j;

      for(int i = 0; i < $size(idx); i++) begin
        j = idx[i];

        expected[j] = tmp[i];   dut.backdoorWr(addr +j, expected[j]);

        $display("[%m][NOTE] at %0d cycle, backdoorWr, addr=0x%0x, data=0x%0x",
                             cntClk, addr +j, expected[j]);
      end
    end

    for(int i=0; i < 8; i++) begin
      data = dut.backdoorRd(addr +i);

      if(data != expected[i]) begin
        $display("[%m][ERROR] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x, while expecting 0x%0x",
                               cntClk, addr +i, data, expected[i]);
      end //if
      else begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x",
                               cntClk, addr +i, data);
      end //else
    end //for i

    idle(100);
  end/*}}}*/

  idle(100);

  $display("[%m] ### Simulation End, Test case(s): %0d ###", cntTest);
  $finish();
end //p_stimulus

`undef  CASE
/*}}}*/

`undef __TESTBENCH_NAME__
endmodule //tb_axi4SubordinateModel
/*}}}*/
//synopsys translate_on

`endif//__EN_BUILT_IN_TEST_BENCH__
`endif//SYNTHESIS
/*}}}*/

//vim60:fdm=marker
//vim60:ts=4
//vim60:sw=4
`endprotect128
