module template #(
    parameter ctrl_addrWidth = 32,
    parameter ctrl_dataWidth = 32,
    parameter data_addrWidth = 32,
    parameter data_dataWidth = 32,
    parameter data_wrWidth   = 4,
    parameter test_iWidth    = 32,
    parameter test_oWidth    = 32,
    parameter test_ioWidth   = 32
) (
// Clock/Reset
   input                              t_PCLK         ,
   input                              t_PRESETn      ,
   input                              t_ACLK         ,
   input                              t_ARESETn      ,

// APB Interface
   input    [ctrl_addrWidth-1:0]      t_PADDR        ,
   input                              t_PSEL         ,
   input                              t_PENABLE      ,
   input                              t_PWRITE       ,
   input    [ctrl_dataWidth-1:0]      t_PWDATA       ,
   output   [ctrl_dataWidth-1:0]      t_PRDATA       ,

// Interrupt Interfadce
   output                      [03:0] t_IRQ          ,

// Test I/O
   output           [test_oWidth-1:0] t_TEST_OUT     ,
   output           [test_oWidth-1:0] t_TEST_OEN     ,
   input            [test_iWidth-1:0] t_TEST_IN      ,
   inout           [test_ioWidth-1:0] t_TEST_IO
);
/////////////////////////////////////////////////////////////////////////
// APB Slave Parts                                                     //
/////////////////////////////////////////////////////////////////////////
   reg    [ctrl_dataWidth-1:0] r_reg_0    ;
   reg    [ctrl_dataWidth-1:0] r_reg_1    ;
// reg    [ctrl_dataWidth-1:0] r_reg_2    ;
   reg    [ctrl_dataWidth-1:0] r_reg_3    ;

   wire   [ctrl_dataWidth-1:0] r_rdata    ;

   wire                        w_reg_0_wr ;
   wire                        w_reg_1_wr ;
// wire                        w_reg_2_wr ;
   wire                        w_reg_3_wr ;

   wire                        w_reg_0_rd ;
   wire                        w_reg_1_rd ;
   wire                        w_reg_2_rd ;
   wire                        w_reg_3_rd ;


// DECODE SFR ACCESS
   assign w_reg_0_wr = t_PSEL &  t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h0) ;
   assign w_reg_1_wr = t_PSEL &  t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h1) ;
// assign w_reg_2_wr = t_PSEL &  t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h2) ;
   assign w_reg_3_wr = t_PSEL &  t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h3) ;

   assign w_reg_0_rd = t_PSEL & ~t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h0) ;
   assign w_reg_1_rd = t_PSEL & ~t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h1) ;
   assign w_reg_2_rd = t_PSEL & ~t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h2) ;
   assign w_reg_3_rd = t_PSEL & ~t_PWRITE & t_PENABLE & (t_PADDR[3:2] == 2'h3) ;

// SFR READ ACCESS
   assign t_PRDATA = (w_reg_0_rd) ? r_reg_0   :
                     (w_reg_1_rd) ? r_reg_1   :
//                   (w_reg_2_rd) ? r_reg_2   :
                     (w_reg_2_rd) ? t_TEST_IN :
                     (w_reg_3_rd) ? r_reg_3   : {ctrl_dataWidth{1'b0}};

// SFR WRITE ACCESS
   always @(negedge t_PRESETn or posedge t_PCLK) begin
       if (!t_PRESETn)      begin r_reg_0 <= {ctrl_dataWidth{1'b0}}; end
       else if (w_reg_0_wr) begin r_reg_0 <= t_PWDATA;          end
   end

   always @(negedge t_PRESETn or posedge t_PCLK) begin
       if (!t_PRESETn)      begin r_reg_1 <= {ctrl_dataWidth{1'b0}}; end
       else if (w_reg_1_wr) begin r_reg_1 <= t_PWDATA;          end
   end

// always @(negedge t_PRESETn or posedge t_PCLK) begin
//     if (!t_PRESETn)      begin r_reg_2 <= {ctrl_dataWidth{1'b0}}; end
//     else if (w_reg_2_wr) begin r_reg_2 <= t_PWDATA;          end
// end

   always @(negedge t_PRESETn or posedge t_PCLK) begin
       if (!t_PRESETn)      begin r_reg_3 <= {ctrl_dataWidth{1'b0}}; end
       else if (w_reg_3_wr) begin r_reg_3 <= t_PWDATA;          end
   end

/////////////////////////////////////////////////////////////////////////
// Interrupt Parts                                                     //
/////////////////////////////////////////////////////////////////////////
   assign t_IRQ[0] = &r_reg_0;
   assign t_IRQ[1] = 1'b0;
   assign t_IRQ[2] = 1'b0;
   assign t_IRQ[3] = 1'b0;


/////////////////////////////////////////////////////////////////////////
// Test IO Parts                                                       //
/////////////////////////////////////////////////////////////////////////
   assign t_TEST_OUT[test_oWidth-1:0] = r_reg_0[ctrl_dataWidth-1:0];
   assign t_TEST_OEN[test_oWidth-1:0] = r_reg_1[ctrl_dataWidth-1:0];


endmodule
