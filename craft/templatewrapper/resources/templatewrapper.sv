/**
  *
  * It is necessary to create a scalar wrapper with a DUH tool to attach IP to a chissel-based SkeletonDUT when user try to do IP onboarding,
  * and it can be a problem with bus interface connection between SkeletonDUT and IP due to the pin name of the IP.
  * However, you do not need to worry about such a problem anymore if you use this verilog wrapper.
  * This verilog warpper was already onboarded in this template. so you just need to instance the IP inside this warpper and connect the pins each other.
  *
  * IPs with abb slave interface and up to 4 interrupt pins can be easily onboarded using this verilog wrapper.
  *
  */
module templatewrapper
(
// Clock/Reset
   input  wire         CLK__Control          ,
   input  wire         RSTN__APB__presetn    ,
   input  wire         CLK__Data             ,
   input  wire         RSTN__AXI4__presetn   ,

// APB Interface
   input  wire [031:0] APB__Control__PADDR   ,
   input  wire         APB__Control__PSEL    ,
   input  wire         APB__Control__PENABLE ,
   input  wire         APB__Control__PWRITE  ,
   input  wire [031:0] APB__Control__PWDATA  ,
   output wire [031:0] APB__Control__PRDATA  ,
   input  wire         APB__Control__PCLKEN  , // ignored
   input  wire [003:0] APB__Control__PPROT   ,
   input  wire [003:0] APB__Control__PSTRB   ,
   output wire         APB__Control__PREADY  ,
   output wire         APB__Control__PSLVERR ,

// AXI4-Lite Interface
   input  wire [003:0]  AXI4__Data__AWID     ,
   input  wire [031:0]  AXI4__Data__AWADDR   ,
   input  wire [007:0]  AXI4__Data__AWLEN    ,
   input  wire [002:0]  AXI4__Data__AWSIZE   ,
   input  wire [001:0]  AXI4__Data__AWBURST  ,
   input  wire          AXI4__Data__AWLOCK   ,
   input  wire [003:0]  AXI4__Data__AWCACHE  ,
   input  wire [002:0]  AXI4__Data__AWPROT   ,
   input  wire [003:0]  AXI4__Data__AWQOS    ,
   input  wire [003:0]  AXI4__Data__AWREGION ,
   input  wire          AXI4__Data__AWUSER   ,
   input  wire          AXI4__Data__AWVALID  ,
   output wire          AXI4__Data__AWREADY  ,
   input  wire [063:0]  AXI4__Data__WDATA    ,
   input  wire [015:0]  AXI4__Data__WSTRB    ,
   input  wire          AXI4__Data__WLAST    ,
   input  wire          AXI4__Data__WVALID   ,
   output wire          AXI4__Data__WREADY   ,
   input  wire          AXI4__Data__BREADY   ,
   output wire [003:0]  AXI4__Data__BID      ,
   output wire [001:0]  AXI4__Data__BRESP    ,
   output wire          AXI4__Data__BUSER    ,
   output wire          AXI4__Data__BVALID   ,
   input  wire [003:0]  AXI4__Data__ARID     ,
   input  wire [031:0]  AXI4__Data__ARADDR   ,
   input  wire [007:0]  AXI4__Data__ARLEN    ,
   input  wire [002:0]  AXI4__Data__ARSIZE   ,
   input  wire [001:0]  AXI4__Data__ARBURST  ,
   input  wire          AXI4__Data__ARLOCK   ,
   input  wire [003:0]  AXI4__Data__ARCACHE  ,
   input  wire [002:0]  AXI4__Data__ARPROT   ,
   input  wire [003:0]  AXI4__Data__ARQOS    ,
   input  wire [003:0]  AXI4__Data__ARREGION ,
   input  wire          AXI4__Data__ARUSER   ,
   input  wire          AXI4__Data__ARVALID  ,
   output wire          AXI4__Data__ARREADY  ,
   output wire [003:0]  AXI4__Data__RID      ,
   output wire [063:0]  AXI4__Data__RDATA    ,
   output wire [001:0]  AXI4__Data__RRESP    ,
   output wire          AXI4__Data__RLAST    ,
   output wire          AXI4__Data__RUSER    ,
   output wire          AXI4__Data__RVALID   ,
   input  wire          AXI4__Data__RREADY   ,

// Interrupts
   output wire          IRQ__t0              ,
   output wire          IRQ__t1              ,
   output wire          IRQ__t2              ,
   output wire          IRQ__t3              ,

// Test IOs
   output wire [031:0]  IO__tOEN             ,
   output wire [031:0]  IO__tOUT             ,
   input  wire [031:0]  IO__tIN              ,
   inout  wire [031:0]  IO__tIO
);
//
// w_oi_* > signal direction : port --> instance module
// w_io_* > signal direction : port <-- instance module
// w_bb_* > signal direction : port <-> instance module (bidirectional)
//
   wire               w_oi_PCLK             ;
   wire               w_oi_PRESETn          ;
   wire       [031:0] w_oi_PADDR            ;
   wire               w_oi_PSEL             ;
   wire               w_oi_PENABLE          ;
   wire               w_oi_PWRITE           ;
   wire       [031:0] w_oi_PWDATA           ;
   wire       [031:0] w_io_PRDATA           ;
   wire       [003:0] w_oi_IRQ              ;
   wire               w_oi_PCLKEN           ;
   wire       [003:0] w_oi_PPROT            ;
   wire       [003:0] w_oi_PSTRB            ;
   wire               w_io_PREADY           ;
   wire               w_io_PSLVERR          ;

   wire       [003:0] w_oi_awid             ;
   wire       [031:0] w_oi_awaddr           ;
   wire       [007:0] w_oi_awlen            ;
   wire       [002:0] w_oi_awsize           ;
   wire       [001:0] w_oi_awburst          ;
   wire               w_oi_awlock           ;
   wire       [003:0] w_oi_awcache          ;
   wire       [002:0] w_oi_awprot           ;
   wire       [003:0] w_oi_awqos            ;
   wire       [003:0] w_oi_awregion         ;
   wire               w_oi_awuser           ;
   wire               w_oi_awvalid          ;
   wire       [063:0] w_oi_wdata            ;
   wire       [015:0] w_oi_wstrb            ;
   wire               w_oi_wlast            ;
   wire               w_oi_wvalid           ;
   wire               w_oi_bready           ;
   wire       [003:0] w_oi_arid             ;
   wire       [031:0] w_oi_araddr           ;
   wire       [007:0] w_oi_arlen            ;
   wire       [002:0] w_oi_arsize           ;
   wire       [001:0] w_oi_arburst          ;
   wire               w_oi_arlock           ;
   wire       [003:0] w_oi_arcache          ;
   wire       [002:0] w_oi_arprot           ;
   wire       [003:0] w_oi_arqos            ;
   wire       [003:0] w_oi_arregion         ;
   wire               w_oi_aruser           ;
   wire               w_oi_arvalid          ;
   wire               w_oi_rready           ;
   wire               w_io_awready          ;
   wire               w_io_wready           ;
   wire       [003:0] w_io_bid              ;
   wire       [001:0] w_io_bresp            ;
   wire               w_io_buser            ;
   wire               w_io_bvalid           ;
   wire               w_io_arready          ;
   wire       [003:0] w_io_rid              ;
   wire       [063:0] w_io_rdata            ;
   wire       [001:0] w_io_rresp            ;
   wire               w_io_rlast            ;
   wire               w_io_ruser            ;
   wire               w_io_rvalid           ;

   wire       [003:0] w_io_IRQ              ;
   wire       [031:0] w_io_TEST_OUT         ;
   wire       [031:0] w_io_TEST_OEN         ;
   wire       [031:0] w_oi_TEST_IN          ;
   wire       [031:0] w_bb_TEST_IO          ;

template #(.ctrl_addrWidth(32), .ctrl_dataWidth(32), .test_iWidth(32), .test_oWidth(32), .test_ioWidth(32) ) u0_template
(
 .t_PCLK            ( w_oi_PCLK              ), // input
 .t_PRESETn         ( w_oi_PRESETn           ), // input
 .t_PADDR           ( w_oi_PADDR     [031:0] ), // input  [031:0]
 .t_PSEL            ( w_oi_PSEL              ), // input
 .t_PENABLE         ( w_oi_PENABLE           ), // input
 .t_PWRITE          ( w_oi_PWRITE            ), // input
 .t_PWDATA          ( w_oi_PWDATA    [031:0] ), // input  [031:0]
 .t_PRDATA          ( w_io_PRDATA    [031:0] ), // output [031:0]
 .t_IRQ             ( w_io_IRQ       [003:0] ), // output [003:0]
 .t_TEST_OUT        ( w_io_TEST_OUT  [031:0] ), // output [031:0]
 .t_TEST_OEN        ( w_io_TEST_OEN  [031:0] ), // output [031:0]
 .t_TEST_IN         ( w_oi_TEST_IN   [031:0] ), // input  [031:0]
 .t_TEST_IO         ( w_bb_TEST_IO   [031:0] )  // inout  [031:0]
);

// !!!! 'REGION_BASE' must be same the 'base address' of this module and REGION_SIZE must not exceed 'size' !!!!
axi4SubordinateModel #(.WIDTH_WID(4), .WIDTH_RID(4), .WIDTH_ADDR(32), .WIDTH_DATA(64), .WIDTH_STRB(16), .WIDTH_WUSER(1), .WIDTH_RUSER(1), .REGION_BASE('h90001000), .REGION_SIZE('h1000)) u1_template
(
 .clk               ( w_oi_clk               ), // input
 .rst_n             ( w_oi_rst_n             ), // input
 .awid              ( w_oi_awid      [003:0] ), // input  [003:0]
 .awaddr            ( w_oi_awaddr    [031:0] ), // input  [031:0]
 .awlen             ( w_oi_awlen     [007:0] ), // input  [007:0]
 .awsize            ( w_oi_awsize    [002:0] ), // input  [002:0]
 .awburst           ( w_oi_awburst   [001:0] ), // input  [001:0]
 .awlock            ( w_oi_awlock            ), // input
 .awcache           ( w_oi_awcache   [003:0] ), // input  [003:0]
 .awprot            ( w_oi_awprot    [002:0] ), // input  [002:0]
 .awqos             ( w_oi_awqos     [003:0] ), // input  [003:0]
 .awregion          ( w_oi_awregion  [003:0] ), // input  [003:0]
 .awuser            ( w_oi_awuser            ), // input
 .awvalid           ( w_oi_awvalid           ), // input
 .awready           ( w_io_awready           ), // output
 .wdata             ( w_oi_wdata     [063:0] ), // input  [063:0]
 .wstrb             ( w_oi_wstrb     [015:0] ), // input  [015:0]
 .wlast             ( w_oi_wlast             ), // input
 .wvalid            ( w_oi_wvalid            ), // input
 .wready            ( w_io_wready            ), // output
 .bid               ( w_io_bid       [003:0] ), // output [003:0]
 .bresp             ( w_io_bresp     [001:0] ), // output [001:0]
 .buser             ( w_io_buser             ), // output
 .bvalid            ( w_io_bvalid            ), // output
 .bready            ( w_oi_bready            ), // input
 .arid              ( w_oi_arid      [003:0] ), // input  [003:0]
 .araddr            ( w_oi_araddr    [031:0] ), // input  [031:0]
 .arlen             ( w_oi_arlen     [007:0] ), // input  [007:0]
 .arsize            ( w_oi_arsize    [002:0] ), // input  [002:0]
 .arburst           ( w_oi_arburst   [001:0] ), // input  [001:0]
 .arlock            ( w_oi_arlock            ), // input
 .arcache           ( w_oi_arcache   [003:0] ), // input  [003:0]
 .arprot            ( w_oi_arprot    [002:0] ), // input  [002:0]
 .arqos             ( w_oi_arqos     [003:0] ), // input  [003:0]
 .arregion          ( w_oi_arregion  [003:0] ), // input  [003:0]
 .aruser            ( w_oi_aruser            ), // input
 .arvalid           ( w_oi_arvalid           ), // input
 .arready           ( w_io_arready           ), // output
 .rid               ( w_io_rid       [003:0] ), // output [003:0]
 .rdata             ( w_io_rdata     [063:0] ), // output [063:0]
 .rresp             ( w_io_rresp     [001:0] ), // output [001:0]
 .rlast             ( w_io_rlast             ), // output
 .ruser             ( w_io_ruser             ), // output
 .rvalid            ( w_io_rvalid            ), // output
 .rready            ( w_oi_rready            )  // input 
);

/////////////////////////////////////////////////////////////////////////
assign  w_oi_PCLK                     =  CLK__Control                  ;
assign  w_oi_PRESETn                  =  RSTN__APB__presetn            ;
assign  w_oi_clk                      =  CLK__Data                     ;
assign  w_oi_rst_n                    =  RSTN__AXI4__presetn           ;

assign  w_oi_PADDR           [031:0]  =  APB__Control__PADDR   [031:0] ;
assign  w_oi_PSEL                     =  APB__Control__PSEL            ;
assign  w_oi_PENABLE                  =  APB__Control__PENABLE         ;
assign  w_oi_PWRITE                   =  APB__Control__PWRITE          ;
assign  w_oi_PWDATA          [031:0]  =  APB__Control__PWDATA  [031:0] ;
assign  APB__Control__PRDATA [031:0]  =           w_io_PRDATA  [031:0] ;

assign  w_oi_awid            [003:0]  =  AXI4__Data__AWID      [003:0] ;
assign  w_oi_awaddr          [031:0]  =  AXI4__Data__AWADDR    [031:0] ;
assign  w_oi_awlen           [007:0]  =  AXI4__Data__AWLEN     [007:0] ;
assign  w_oi_awsize          [002:0]  =  AXI4__Data__AWSIZE    [002:0] ;
assign  w_oi_awburst         [001:0]  =  AXI4__Data__AWBURST   [001:0] ;
assign  w_oi_awlock                   =  AXI4__Data__AWLOCK            ;
assign  w_oi_awcache         [003:0]  =  AXI4__Data__AWCACHE   [003:0] ;
assign  w_oi_awprot          [002:0]  =  AXI4__Data__AWPROT    [002:0] ;
assign  w_oi_awqos           [003:0]  =  AXI4__Data__AWQOS     [003:0] ;
assign  w_oi_awregion        [003:0]  =  AXI4__Data__AWREGION  [003:0] ;
assign  w_oi_awuser                   =  AXI4__Data__AWUSER            ;
assign  w_oi_awvalid                  =  AXI4__Data__AWVALID           ;
assign  w_oi_wdata           [063:0]  =  AXI4__Data__WDATA     [063:0] ;
assign  w_oi_wstrb           [015:0]  =  AXI4__Data__WSTRB     [015:0] ;
assign  w_oi_wlast                    =  AXI4__Data__WLAST             ;
assign  w_oi_wvalid                   =  AXI4__Data__WVALID            ;
assign  w_oi_bready                   =  AXI4__Data__BREADY            ;
assign  w_oi_arid            [003:0]  =  AXI4__Data__ARID      [003:0] ;
assign  w_oi_araddr          [031:0]  =  AXI4__Data__ARADDR    [031:0] ;
assign  w_oi_arlen           [007:0]  =  AXI4__Data__ARLEN     [007:0] ;
assign  w_oi_arsize          [002:0]  =  AXI4__Data__ARSIZE    [002:0] ;
assign  w_oi_arburst         [001:0]  =  AXI4__Data__ARBURST   [001:0] ;
assign  w_oi_arlock                   =  AXI4__Data__ARLOCK            ;
assign  w_oi_arcache         [003:0]  =  AXI4__Data__ARCACHE   [003:0] ;
assign  w_oi_arprot          [002:0]  =  AXI4__Data__ARPROT    [002:0] ;
assign  w_oi_arqos           [003:0]  =  AXI4__Data__ARQOS     [003:0] ;
assign  w_oi_arregion        [003:0]  =  AXI4__Data__ARREGION  [003:0] ;
assign  w_oi_aruser                   =  AXI4__Data__ARUSER            ;
assign  w_oi_arvalid                  =  AXI4__Data__ARVALID           ;
assign  w_oi_rready                   =  AXI4__Data__RREADY            ;
assign  AXI4__Data__AWREADY           =  w_io_awready                  ;
assign  AXI4__Data__WREADY            =  w_io_wready                   ;
assign  AXI4__Data__BID      [003:0]  =  w_io_bid              [003:0] ;
assign  AXI4__Data__BRESP    [001:0]  =  w_io_bresp            [001:0] ;
assign  AXI4__Data__BUSER             =  w_io_buser                    ;
assign  AXI4__Data__BVALID            =  w_io_bvalid                   ;
assign  AXI4__Data__ARREADY           =  w_io_arready                  ;
assign  AXI4__Data__RID      [003:0]  =  w_io_rid              [003:0] ;
assign  AXI4__Data__RDATA    [063:0]  =  w_io_rdata            [063:0] ;
assign  AXI4__Data__RRESP    [001:0]  =  w_io_rresp            [001:0] ;
assign  AXI4__Data__RLAST             =  w_io_rlast                    ;
assign  AXI4__Data__RUSER             =  w_io_ruser                    ;
assign  AXI4__Data__RVALID            =  w_io_rvalid                   ;

assign  IRQ__t0                       =  w_io_IRQ                  [0] ;
assign  IRQ__t1                       =  w_io_IRQ                  [1] ;
assign  IRQ__t2                       =  w_io_IRQ                  [2] ;
assign  IRQ__t3                       =  w_io_IRQ                  [3] ;

assign  APB__Control__PREADY          =  1'b1                          ;
assign  APB__Control__PSLVERR         =  1'b0                          ;

assign  IO__tOUT              [31:0]  =  w_io_TEST_OUT          [31:0] ;
assign  IO__tOEN              [31:0]  =  w_io_TEST_OEN          [31:0] ;
assign  w_oi_TEST_IN          [31:0]  =  IO__tIN                [31:0] ;
assign  IO__tIO               [31:0]  =  w_bb_TEST_IO           [31:0] ;
/////////////////////////////////////////////////////////////////////////

endmodule
