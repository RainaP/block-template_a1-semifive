`protect128
// ############################################################################
//
// Generic Bus Subordinate Model
//
//  Written by kieran.kim@semifive.com
//
// Brief description
//  module that provides bus subordinate features for functional verification
//
// ############################################################################
//
// TODO
//  2021/04/10  (kieran.kim)
//      Add     log file feature
//      Add     load file feature
//      Add     bandwidth calculation
//      Add     toggling calcuation
//
//  2021/04/11  (kieran.kim)
//      Add     standardized messages header for their severity
//      Extend  multiIdConvey to support user-define side-band tag
//
// ############################################################################
//
// RELEASE NOTES
//  2021/04/22  (kieran.kim)
//      Add     backdoor accessibility
//
//  2021/04/11  (kieran.kim)
//      Add     license check routine
//
//  2021/04/09  (kieran.kim)
//      Init
//
// ############################################################################
//
// Implementation Note
//
// [Access Granularity]
//  All accesses are handled in transaction level
//  also, all transactions will be handled in a atomic manner
//
// [Ordering Model]
//  All transactions will be concluded when it is requested by txWr(), txRd()
//  But, corresponding response wil be thrown with latency
//
// [Supported Arbitration Schemes]
//  FCFS: first-come-first-serve
//  RAND: random
//  RR  : round-robin
//
// [Supported Latency Models and Configuration]
//  UNIFORM : max = LAT_WR/RD_MAX_DEV, min = LAT_WR/RD_MIN_AVG
//  ERLANG  : number of stages = LAT_WR/RD_MIN_AVG, average = LAT_WR/RD_MIN_AVG
//  GAUSSIAN: deviation = LAT_WR/RD_MAX_DEV, average = LAT_WR/RD_MIN_AVG
//  POISSON : average = LAT_WR/RD_MIN_AVG
//
// [API]
//  This module provides two tasks: txWr() & txRd()
//  whereas it is assumed that parent module has two others: rxWr() & rxRd()
//
//    txWr: execute write transactions
//      int                    txId         //transaction ID
//      longint                bAddrStart   //starting address in Bytes
//      longint                bAddrEnd     //  ending address in Bytes
//      logic [WIDTH_DATA-1:0] data[$]      //write data
//      logic [WIDTH_STRB-1:0] strb[$]      //Byte strobe
//
//    txRd: execute read transactions
//      int                    txId,        //transaction ID
//      longint                bAddrStart,  //starting address in Bytes
//      longint                bAddrEnd     //  ending address in Bytes
//
//    rxWr: handle write response
//      int                    txId         //transaction ID
//
//    rxRd: handle read response
//      int                    txId         //transaction ID
//      logic [WIDTH_DATA-1:0] data[$]      //read data
//
//  For backdoor access, there are two additional tasks
//    backdoorWr: backdoor write
//      longint                bAddr        //Byte address to access
//      byte                   data         //write data
//
//    backdoorRd: backdoor read returning 8-bit
//      longint                bAddr        //Byte address to access
//
// [Outstanding Number Control]
//  This module has no limit in terms of multiple outstandings
//  Therefore, the parent module MUST take care of controlling outstandings
//  numbers.
//
// ############################################################################
//

module busSubordinateModel #(
//data transfer
parameter WIDTH_DATA      = 128,          //bit width of data
parameter WIDTH_STRB      = WIDTH_DATA/8, //bit width of data mask (MNEMONIC)

//arbitration
parameter TYPE_ARB_WR     = "FCFS",       //arbitration type for write ID
                                          //  "FCFS", "RAND" or "RR"
parameter TYPE_ARB_RD     = "FCFS",       //arbitration type for  read ID
                                          //  "FCFS", "RAND" or "RR"

//latency
parameter TYPE_LATENCY_WR = "UNIFORM",    //latency model for write
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
parameter LAT_WR_MAX_DEV  = 4,            //latency model param for write
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
parameter LAT_WR_MIN_AVG  = 4,            //latency model param for write
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

parameter TYPE_LATENCY_RD = "UNIFORM",    //latency model for read
                                          //  "UNIFORM", "ERLANG", "GAUSSIAN"
                                          //  or "POISSON",
parameter LAT_RD_MAX_DEV  = 4,            //latency model param for read
                                          //  UNIFORM : max
                                          //  ERLANG  : k stage
                                          //  GAUSSIAN: dev
                                          //  POISSON : NO MEANING
parameter LAT_RD_MIN_AVG  = 4,            //latency model param for read
                                          //  UNIFORM : min
                                          //  ERLANG  : mean
                                          //  GAUSSIAN: mean
                                          //  POISSON : mean

//logging
parameter MAX_MESSAGE     = 1000,         //max num of warning/error messages
parameter EN_WAR_CHECK    = 1             //0: disable read-prior-to-write check (Write-After-Read)
)
(
//system
input  logic  clk,      //operation clock
input  logic  rst_n     //asynchronous active-low reset
); //busSubordinateModel

////////////////////////////////////////////////////////////////////////////////
//Signal Declaration
/*{{{*/
localparam SHIFT_WORD_ADDR = $clog2(WIDTH_DATA/8);  //num bits to shift to calc Byte addr

typedef bit                    t_respWr;      //response type for write
typedef logic [WIDTH_DATA-1:0] t_respRd[$];   //response type for read

longint now;  //free-run cycle counter

//class to handle delay per ID
class multiIdConvey #(/*{{{*/
  parameter type T  = integer,  //type of packet to handle
  parameter SEED    = 0,        //seed for randomization
  parameter ARB     = "FCFS",   //type of arbitration
                                //  "FCFS", "RAND", or "RR"
  parameter DELAY   = "UNIFORM",//type of latency modeling
                                //  "UNIFORM", "GAUSSIAN", "POISSON", or "ERLANG"
  parameter MAX_DEV = 4,        //latency modeling param
                                //  UNIFORM : max
                                //  ERLANG  : k stage
                                //  GAUSSIAN: dev
                                //  POISSON : NO MEANING
  parameter MIN_AVG = 4         //latency modeling param
                                //  UNIFORM : min
                                //  ERLANG  : mean
                                //  GAUSSIAN: mean
                                //  POISSON : mean
);/*}}}*/
//body/*{{{*/
  typedef struct {longint ticket; T parcel;} t_tray;

  local t_tray convey[int][$];  //convey per ID
  local int       idQ     [$];  //ID queue for arbitration
  local int      seed;          //seed for randomization

  function new();/*{{{*/
    seed = SEED;

    if((ARB != "FCFS") && (ARB != "RAND") && (ARB != "RR")) begin
      $display("[%m][FATAL] unknown arbitration type: '%s'", ARB);
      $finish;
    end //if

    if((DELAY != "UNIFORM") && (DELAY != "GAUSSIAN")
    && (DELAY != "POISSON") && (DELAY != "ERLANG"))
    begin
      $display("[%m][FATAL] unkown latency type: '%s'", DELAY);
      $finish;
    end //if
  endfunction/*}}}*/

  //push a packet
  function void push(int id, T packet);/*{{{*/
    t_tray tray;

    tray.parcel = packet;
    tray.ticket = now +getLatency();

    convey[id].push_back(tray);
           idQ.push_back(id);
  endfunction //push/*}}}*/

  //pop a packet, iff available
  function bit pop(int id, ref T packet);/*{{{*/
    t_tray   tray;

    //NOTE: out-of-index error may cause if something wrong
    if(0 == convey[id].size()) return 1'b0;   //empty convey

    tray   = convey[id][0];
    packet = tray.parcel;

    if(tray.ticket > now) return 1'b0;        //not available yet

    convey[id].pop_front();
    popId (id);

    return 1'b1;
  endfunction //pop/*}}}*/

  //pop an ID from idQ
  local function void popId(int id);/*{{{*/
    int len;

    len = idQ.size();

    for(int i = 0; i < len; i++) begin
      if(id == idQ[i]) begin
        idQ.delete(i);  //remove oldest one
        break;
      end //if
    end //for i
  endfunction //popId/*}}}*/

  //see if there is tray which may not available yet
  function bit isEmpty();/*{{{*/
    return (0 == idQ.size());
  endfunction //isEmpty/*}}}*/

  //return arbitration result among IDs
  //NOTE: selected ID may not eligile for poping it out
  function int pickId();/*{{{*/
    static int prev = -1;   //memo the result for round-robin
           int arb;         //arbitration result

          if(ARB == "FCFS") begin arb = idQ[0];
    end //if
    else  if(ARB == "RAND") begin arb = $urandom_range(idQ.size() -1, 0);
                                  arb = idQ[arb];
    end //else if
    else/*if(ARB == "RR")*/ begin if(prev < 0) begin
                                    arb = idQ[0];
                                  end //if
                                  else begin
                                    int ordered[$] = idQ;
                                    ordered.sort();

                                    //NOTE: this could be selected in a wrap-around case
                                    arb = ordered[0];

                                    foreach(ordered[i])
                                      if(ordered[i] > prev) begin
                                        arb = ordered[i];
                                        break;
                                      end //if
                                  end //else
    end //else

    prev = arb;

    return arb;

  endfunction //pickId/*}}}*/

  //get a latency value
  local function int getLatency();/*{{{*/
    int latency;  //calculated result

          if(DELAY == "UNIFORM")  latency = $dist_uniform(seed, MIN_AVG, MAX_DEV);  //min, max
    else  if(DELAY == "ERLANG")   latency = $dist_erlang (seed, MAX_DEV, MIN_AVG);  //k stage, mean
    else  if(DELAY == "GAUSSIAN") latency = $dist_normal (seed, MIN_AVG, MAX_DEV);  //mean, deviation
    else/*if(DELAY == "POISSON")*/latency = $dist_poisson(seed, MIN_AVG);           //mean only

    return latency;
  endfunction //getLatency/*}}}*/

endclass //multiIdConvey/*}}}*/

multiIdConvey #(.T(t_respWr), .ARB(TYPE_ARB_WR),
                .DELAY(TYPE_LATENCY_WR), .MAX_DEV(LAT_WR_MAX_DEV), .MIN_AVG(LAT_WR_MIN_AVG))
                trackWr = new();

multiIdConvey #(.T(t_respRd), .ARB(TYPE_ARB_RD),
                .DELAY(TYPE_LATENCY_RD), .MAX_DEV(LAT_RD_MAX_DEV), .MIN_AVG(LAT_RD_MIN_AVG))
                trackRd = new();

semaphore mutex;          //semaphore to access mem
bit       smudge[int];    //keeping whether an word address had been written

event     tick_WAR;       //visualize when read-prior-to-write happen

event     tick_BWR;       //visualize when backdoor write occur
event     tick_BRD;       //visualize when backdoor  read occur

int       cntMsg;         //number of error/warnning message
int       cntErr;         //number of error messages
int       cntWrn;         //number of warning messages

logic [WIDTH_DATA-1:0] mem [int];   //memory component to access

/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Instantiation
/*{{{*/
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Procedures
/*{{{*/
initial begin: param_report/*{{{*/
//report parameters
`define STR(str) `"str`"    //stringify
`define PARAM_REPORT_HEX(X,N) $display("[NOTE] @%m %s: 0x%s", `STR(X), $psprintf({"%0",$psprintf("%0d",(N)),"x"}, (X)))
`define PARAM_REPORT_INT(X)   $display("[NOTE] @%m %s: %0d",  `STR(X), (X))
`define PARAM_REPORT_STR(X)   $display("[NOTE] @%m %s:'%s'",  `STR(X), (X))

`PARAM_REPORT_INT(WIDTH_DATA);      //bit width of data
`PARAM_REPORT_INT(WIDTH_STRB);      //bit width of data mask (MNEMONIC)

`PARAM_REPORT_STR(TYPE_ARB_WR);     //arbitration type for write ID
`PARAM_REPORT_STR(TYPE_ARB_RD);     //arbitration type for  read ID

`PARAM_REPORT_STR(TYPE_LATENCY_WR); //latency model for write
`PARAM_REPORT_INT(LAT_WR_MAX_DEV);  //latency model param for write
`PARAM_REPORT_INT(LAT_WR_MIN_AVG);  //latency model param for write

`PARAM_REPORT_STR(TYPE_LATENCY_RD); //latency model for read
`PARAM_REPORT_INT(LAT_RD_MAX_DEV);  //latency model param for read
`PARAM_REPORT_INT(LAT_RD_MIN_AVG);  //latency model param for read

`PARAM_REPORT_INT(MAX_MESSAGE);     //max num of warning/error messages
`PARAM_REPORT_INT(EN_WAR_CHECK);    //0: disable read-prior-to-write check (Write-After-Read)

`undef PARAM_REPORT_STR
`undef PARAM_REPORT_INT
`undef PARAM_REPORT_HEX
`undef STR
end //param_report/*}}}*/
//Auto-versioniing/*{{{*/
localparam  WIDTH_INT     = 32;           //data width of host interface
localparam  WIDTH_STR_BUF = WIDTH_INT *8; //'*8' for ascii
localparam [WIDTH_STR_BUF-1:0] REV_STR = "$Rev:$"; //revision number or hash key annotated by version control

localparam gitHash = getGitHash(REV_STR);

function int getGitHash;/*{{{*/
input [WIDTH_STR_BUF-1:0] str;

reg   [8-1:0] ch; //buffer for a character
int           i;    //iterator for REV_STR
int           rIdx; //right index for i to start
int           lIdx; //left  index for i to stop
int           result;

lIdx = 8;             //to skip rith '$'
rIdx = lIdx +(8 *7);  //always use 7 characters only

result = 0;

if (REV_STR == {"$Re", "v:$"}) begin
    //return deacafe
    return 'hdeacafe;
end //if

//scaning from left to right
for(i = rIdx; i >= lIdx; i = i -8) begin
    ch = REV_STR[i+:8];
    result = (16*result) +((ch >= 8'h30 && ch <= 8'h39) ? ch[3:0]       //ascii(0) = 0x30, asscii(9) = 0x39
                         : (ch >= 8'h41 && ch <= 8'h46) ? ch[3:0] +4'h9 //ascii(A) = 0x41, asscii(F) = 0x46
                         : (ch >= 8'h61 && ch <= 8'h66) ? ch[3:0] +4'h9 //ascii(a) = 0x61, asscii(f) = 0x66
                         :                                4'h0);
end //for i

getGitHash = result;

endfunction //getGitHash/*}}}*/

function int getVer();
    return gitHash;
endfunction //getVer
/*}}}*/
initial begin //p_lic_check/*{{{*/
    //NOTE: to update 'expire', update year in the banner also
    longint expire  = 1672498800;   //'date -d 2023-01-01 +%s'

    string  unitKey = "busSubordinateModel";

    //print banner
    $display("\n",    //1         2         3         4         5         6         7
            //0123456789012345678901234567890123456789012345678901234567890123456789012345
             "############################################################################\n",
             "#                      Bus Subordinate Model (%07x)                     #\n", gitHash,
             "#                 Copyright 2019-2021 (c) by SEMIFIVE Inc.                 #\n",
             "#                           ALL RIGHTS RESERVED                            #\n",
             "############################################################################\n");

    begin
        //See if license is expired
        string lock;    //lock file name
        string cmd;     //system command
        int    exec;    //system execution result
        int    fp;      //file pointer

        longint today;  //today in seconds

        //NOTE: '%m' cannot be employed since unnamed block in hierarchy CANNOT be resolved here
        lock = $psprintf("._%s%0d_", unitKey, $urandom());      //$display("lock: '%s'", lock);
        cmd  = {"date +%s > ", lock};                           //$display("cmd : '%s'", cmd);

        exec = $system(cmd);                if( exec) $finish;  //$display("PROBE: %s, %0d", `__FILE__, `__LINE__);
        fp   = $fopen(lock, "r");           if(!fp)   $finish;  //$display("PROBE: %s, %0d", `__FILE__, `__LINE__);
        exec = $fscanf(fp, "%d", today);    if(!exec) $finish;  //$display("PROBE: %s, %0d", `__FILE__, `__LINE__);
        $fclose(fp);

        cmd  = $psprintf("\rm -f %s", lock);
        exec = $system(cmd);                if( exec) $finish;  //$display("PROBE: %s, %0d", `__FILE__, `__LINE__);
                                                                //$display("today : %0d", today);
                                                                //$display("expire: %0d", expire);
        if(today >= expire) $finish;
    end
end //p_lic_check/*}}}*/
always @(posedge clk, negedge rst_n) begin: p_now/*{{{*/
  if(!rst_n)  now <= '0;
  else        now <= now +1;
end //p_now/*}}}*/

//do initialization whenever reset is asserted
always @(negedge rst_n) begin: p_init/*{{{*/
  mutex = new(1);

  if(smudge.size() > 0) smudge.delete();
end //p_init/*}}}*/

//handle a write transaction
task automatic txWr(/*{{{*/
  int                              txId,        //transaction ID
  longint                          bAddrStart,  //starting address in Bytes
  longint                          bAddrEnd,    //  ending address in Bytes
  const ref logic [WIDTH_DATA-1:0] data[$],     //write data
  const ref logic [WIDTH_STRB-1:0] strb[$]      //Byte strobe
);
  logic [WIDTH_DATA-1:0] bufWord;     //temp for a word from/to mem
  logic [WIDTH_DATA-1:0] beat;        //temp for data
  logic [WIDTH_STRB-1:0] bulb;        //temp for strb

  longint wAddrStart; //starting address in words
  longint wAddrEnd;   //  ending address in words
  longint wCnt;       //count in words

  longint wItr;       //word address to iterate

  mutex.get();

  wAddrStart = bAddrStart >> SHIFT_WORD_ADDR;
  wAddrEnd   = bAddrEnd   >> SHIFT_WORD_ADDR;

  wCnt       = wAddrEnd -wAddrStart +1;

  //sanity check: consistency among addresses and data counts
  if((wCnt != data.size())
  || (wCnt != strb.size()))
  begin
    $display("[%m][FATAL] inconsistency detected, bAddrStart: 0x%0x, bAddrEnd: 0x%0x, data.size: %0d, strb.size:%0d, expected word count: %0d",
              bAddrStart, bAddrEnd, data.size(), strb.size(), wCnt);
    $finish;
  end //if

  for(longint idx = 0; idx < wCnt; idx++) begin
    wItr    = idx +wAddrStart;

    bufWord = mem [wItr];
    beat    = data[idx];
    bulb    = strb[idx];

    //write a word
    for(int i = 0; i < WIDTH_STRB; i++) begin
      if(bulb[i]) begin
        bufWord[8*i+:8] = beat[8*i+:8];   //Byte strobe
      end //if
    end //for i

    mem   [wItr] = bufWord;
    smudge[wItr] = 1'b1;

    //$display("[%m][DEBUG] (cycle: %0d) addr: 0x%0h, data: 0x%0h", now, bAddrStart +idx*WIDTH_STRB, mem[wAddrStart +idx]);

    //TODO: add write log HERE
  end //for idx

  trackWr.push(txId, 1'b1);

  //TODO: add bandwidth calc HERE

  mutex.put();
endtask //txWr/*}}}*/

//handle a read transaction
task automatic txRd(/*{{{*/
  int       txId,        //transaction ID
  longint   bAddrStart,  //starting address in Bytes
  longint   bAddrEnd     //  ending address in Bytes
);
  t_respRd packet;

  longint wAddrStart; //starting address in words
  longint wAddrEnd;   //  ending address in words
  longint wCnt;       //count in words

  longint wItr;       //word address to iterate

  mutex.get();

  wAddrStart = bAddrStart >> SHIFT_WORD_ADDR;
  wAddrEnd   = bAddrEnd   >> SHIFT_WORD_ADDR;

  wCnt       = wAddrEnd -wAddrStart +1;

  for(longint idx = 0; idx < wCnt; idx++) begin
    wItr = idx +wAddrStart;

    packet.push_back(mem[wItr]);   //read a word

    //$display("[%m][DEBUG] (cycle: %0d) addr: 0x%0h, data: 0x%0h", now, bAddrStart +idx*WIDTH_STRB, mem[wAddrStart +idx]);

    checkWAR(wItr);   //check read-prior-to-write

    //TODO: add read log HERE
  end //for idx

  trackRd.push(txId, packet);

  mutex.put();
endtask //txWr/*}}}*/

//feed-back write response
always @(posedge clk iff rst_n) begin: p_respWr/*{{{*/
  int      id;
  t_respWr pkt;

  if(!trackWr.isEmpty()) begin
    id = trackWr.pickId();

    if(trackWr.pop(id, pkt))
      rxWr(id);     //parent module MUST have rxWr()
  end //if
end //p_respWr/*}}}*/

//feed-back read response
always @(posedge clk iff rst_n) begin: p_respRd/*{{{*/
  int      id;
  t_respRd pkt;

  if(!trackRd.isEmpty()) begin
    id = trackRd.pickId();

    if(trackRd.pop(id, pkt)) begin
      //$display("[%m][DEBUG]", now, id, pkt);

      rxRd(id, pkt);  //parent module MUST have rxRd()
    end //if
  end //if
end //p_respRd/*}}}*/

//backdoor write
function automatic backdoorWr(/*{{{*/
  longint        bAddr,   //Byte address to access
  const ref byte data     //write data
);
  logic [WIDTH_DATA-1:0] bufWord;     //temp for a word from/to mem

  longint wAddr;          //word address
  int     idx;            //byte offset in a word

  wAddr = bAddr >> SHIFT_WORD_ADDR;
  idx   = bAddr[0+:SHIFT_WORD_ADDR];

  mem[wAddr][8*idx+:8] = data;

  smudge[wAddr] = 1'b1;

  ->tick_BWR;
endfunction //backdoorWr/*}}}*/

//backdoor read
function automatic byte backdoorRd(/*{{{*/
  longint        bAddr     //Byte address to access
);
  logic [WIDTH_DATA-1:0] bufWord;     //temp for a word from/to mem

  longint wAddr;          //word address
  int     idx;            //byte offset in a word

  wAddr = bAddr >> SHIFT_WORD_ADDR;
  idx   = bAddr[0+:SHIFT_WORD_ADDR];

  checkWAR(wAddr);

  ->tick_BRD;

  return mem[wAddr][8*idx+:8];

endfunction //backdoorRd/*}}}*/

//see if read-prior-to-write happen (Write-After-Read)
function automatic checkWAR (longint addr);/*{{{*/
  static int cntVio = 0;   //number of violation (static)

  if(!smudge[addr]) begin
    cntVio++; cntErr++; cntMsg++;

    ->tick_WAR;

    if(EN_WAR_CHECK && (cntMsg <= MAX_MESSAGE))
      $display("[%m][ERROR] memory (0x%0h) is read without initialization", addr);
  end //if
endfunction //checkWAR/*}}}*/

final begin: p_summary/*{{{*/
  if(checkWAR.cntVio) $display("[%m][NOTE] number of read-prior-to-write case(s): %0d", checkWAR.cntVio);

  if(cntWrn) $display("[%m][NOTE] number of wranning message(s): %0d", cntWrn);
  if(cntErr) $display("[%m][NOTE] number of error    message(s): %0d", cntErr);
end //p_summary/*}}}*/
/*}}}*/

endmodule //busSubordinateModel

////////////////////////////////////////////////////////////////////////////////
//Built-in Test Bench
/*{{{*/
`ifndef SYNTHESIS
`ifdef __EN_BUILT_IN_TEST_BENCH__

//synopsys translate_off
/*{{{*/
module tb_busSubordinateModel();

`define  __TESTBENCH_NAME__  tb_busSubordinateModel
localparam TESTBENCH_NAME = "tb_busSubordinateModel";

////////////////////////////////////////////////////////////////////////////////
//Parameter List
/*{{{*/
//testbench paramter
parameter TEST_CFG_NAME = "BUILT_IN_TEST";  //test configuration name

//DUT parameter
parameter  WIDTH_DATA      = 128;           //bit width of data
localparam WIDTH_STRB      = WIDTH_DATA/8;  //bit width of data mask (MNEMONIC)
localparam WIDTH_BYTE      = WIDTH_DATA/8;  //bit width of data in Bytes (MNEMONIC)

parameter  TYPE_ARB_WR     = "FCFS";        //arbitration type for write ID
                                            //  "RAND" or "RR" or "FCFS"
parameter  TYPE_ARB_RD     = "FCFS";        //arbitration type for  read ID
                                            //  "RAND" or "RR" or "FCFS"

parameter  TYPE_LATENCY_WR = "UNIFORM";     //latency model for write
                                            //  "UNIFORM"; "ERLANG", "GAUSSIAN"
                                            //  or "POISSON";
parameter  LAT_WR_MAX_DEV  = 4;             //latency model param for write
                                            //  UNIFORM : max
                                            //  ERLANG  : k stage
                                            //  GAUSSIAN: dev
                                            //  POISSON : NO MEANING
parameter  LAT_WR_MIN_AVG  = 4;             //latency model param for write
                                            //  UNIFORM : min
                                            //  ERLANG  : mean
                                            //  GAUSSIAN: mean
                                            //  POISSON : mean

parameter  TYPE_LATENCY_RD = "UNIFORM";     //latency model for read
                                            //  "UNIFORM"; "ERLANG", "GAUSSIAN"
                                            //  or "POISSON";
parameter  LAT_RD_MAX_DEV  = 4;             //latency model param for read
                                            //  UNIFORM : max
                                            //  ERLANG  : k stage
                                            //  GAUSSIAN: dev
                                            //  POISSON : NO MEANING
parameter  LAT_RD_MIN_AVG  = 4;             //latency model param for read
                                            //  UNIFORM : min
                                            //  ERLANG  : mean
                                            //  GAUSSIAN: mean
                                            //  POISSON : mean

parameter  MAX_MESSAGE     = 1000;          //max num of warning/error messages
parameter  EN_WAR_CHECK    = 1;             //0: disable read-prior-to-write check (Write-After-Read)

///////////////////////////////////////////////////////////////////////////////
//Parameter report
initial begin: param_report/*{{{*/
//NOTE: all testbench paramters MUST be reported HERE
//NOTE: all DUT parameters will be reported by the DUT

`define STR(str) `"str`"    //stringify
`define PARAM_REPORT_HEX(X,N) $display("[NOTE] @%m %s: 0x%s", `STR(X), $psprintf({"%0",$psprintf("%0d",(N)),"x"}, (X)))
`define PARAM_REPORT_INT(X)   $display("[NOTE] @%m %s: %0d",  `STR(X), (X))
`define PARAM_REPORT_STR(X)   $display("[NOTE] @%m %s:'%s'",  `STR(X), (X))

`PARAM_REPORT_STR(TEST_CFG_NAME);     //test configuration name

`undef PARAM_REPORT_STR
`undef PARAM_REPORT_INT
`undef PARAM_REPORT_HEX
`undef STR
end //param_report/*}}}*/
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Clock and Reset
/*{{{*/
localparam HALF_PERIOD = 5;   //half preiod of clk in ns

bit oscillator;     //self-running oscillator

bit clk;            //operation clock
bit rst_n;          //active-low reset

longint cntClk = 0; //cycle counter

initial forever #(HALF_PERIOD*1ns) oscillator = ~oscillator;

always_ff @(posedge oscillator) clk    <= ~clk;
always_ff @(posedge clk)        cntClk <=  cntClk +1;

initial begin: p_rst
  repeat (10) @(posedge clk);

  @(posedge clk) rst_n <= 1'b1;
end //p_rst
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Waveform dump: Full dump assuming small DUT
/*{{{*/
`ifndef VCS
`ifndef NCSIM
`define VCS   //default simulator
`endif//NCSIM
`endif//VCS

`ifdef NCSIM/*{{{*/
initial begin: proc_dump_shm
    $display ("[NOTE] @%m Open shm '%s.shm'", TESTBENCH_NAME);
    $shm_open({TESTBENCH_NAME, ".shm"},    //db_name
                ,       //is_sequence_time; dump all value change in a cycle
                ,       //db_size; maximum size in Bytes
                1,      //is_compression
                1024,   //incsize; incremental file size in MBytes
                200);   //incfiles; number of incremental files

    //NOTE: probe node specifier
    //  'A' : all nodes
    //  'S' : all nodes in all instantiation below, except library cells
    //  'C' : all nodes in all instantiation below, including library cells
    //  'M' : memory
    //  'T' : task
    //  'F' : function

    $shm_probe(`__TESTBENCH_NAME__, "AMCTF");
end //proc_dump_shm
`endif//NCSIM/*}}}*/

`ifdef VCS/*{{{*/
initial begin: proc_dump_vpd
    $display ("[NOTE] @%m Open vpd '%s.vpd'", TESTBENCH_NAME);
    $vcdplusfile({TESTBENCH_NAME, ".vpd"});

    $vcdpluson;
end //proc_dump_vpd
`endif//VCS/*}}}*/
/*}}}*/

//////////////////////////////////////////////////////////////////////////////
//Verdi
/*{{{*/
`ifdef  VERDI
initial begin: proc_dump_fsdb
    int chunkSize = 1024;   //1GBytes

    $fsdbDumpfile({TESTBENCH_NAME, ".fsdb"});

    //slice FSDB into chunks
    $fsdbAutoSwitchDumpfile(chunkSize, {TESTBENCH_NAME, ".fsdb"}, 0, {TESTBENCH_NAME, ".log"});

    $fsdbDumpvars(0, TESTBENCH_NAME);

    $display ("[NOTE] @%m Open FSDB '%s.fsdb'", TESTBENCH_NAME);
end //proc_dump_fsdb
`endif//VERDI
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Signal Declaration
/*{{{*/
//write request packet
typedef struct{
  int                    txId;        //transaction ID
  longint                bAddrStart;  //starting address in Bytes
  longint                bAddrEnd;    //  ending address in Bytes
  logic [WIDTH_DATA-1:0] data[$];     //write data
  logic [WIDTH_STRB-1:0] strb[$];     //Byte strobe
} t_reqWr;

//read request packet
typedef struct{
  int                    txId;        //transaction ID
  longint                bAddrStart;  //starting address in Bytes
  longint                bAddrEnd;    //  ending address in Bytes
} t_reqRd;

//write response packet
typedef struct{
  int                    txId;        //transactin ID
} t_rspWr;

//read response packet
typedef struct{
  int                    txId;        //transactin ID
  logic [WIDTH_DATA-1:0] data[$];     //read data
} t_rspRd;

t_rspWr qRspWr[$];  //write response queue
t_rspRd qRspRd[$];  // read response queue

event tick_reqWr;   //request a write
event tick_rspWr;   //get a write response

event tick_reqRd;   //request a read
event tick_rspRd;   //get a read response
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Instantiation
/*{{{*/
busSubordinateModel #(
.WIDTH_DATA      (WIDTH_DATA),        //bit width of data
.WIDTH_STRB      (WIDTH_STRB),        //bit width of data mask (MNEMONIC)

.TYPE_ARB_WR     (TYPE_ARB_WR),       //arbitration type for write ID
.TYPE_ARB_RD     (TYPE_ARB_RD),       //arbitration type for  read ID

.TYPE_LATENCY_WR (TYPE_LATENCY_WR),   //latency model for write
.LAT_WR_MAX_DEV  (LAT_WR_MAX_DEV),    //latency model param for write
.LAT_WR_MIN_AVG  (LAT_WR_MIN_AVG),    //latency model param for write

.TYPE_LATENCY_RD (TYPE_LATENCY_RD),   //latency model for read
.LAT_RD_MAX_DEV  (LAT_RD_MAX_DEV),    //latency model param for read
.LAT_RD_MIN_AVG  (LAT_RD_MIN_AVG),    //latency model param for read

.MAX_MESSAGE     (MAX_MESSAGE),       //max num of warning/error messages
.EN_WAR_CHECK    (EN_WAR_CHECK)       //0: disable read-prior-to-write check (Write-After-Read)
)
dut (
.clk    (clk),    //operation clock
.rst_n  (rst_n)   //asynchronous active-low reset
);
/*}}}*/

////////////////////////////////////////////////////////////////////////////////
//Procedures
/*{{{*/
`define CASE(X) begin nameTest = (X); $display("[%m] ### test-%0d %s", cntTest++, X); end

task automatic idle(int n);/*{{{*/
  repeat(n) @(posedge clk);
endtask //idle/*}}}*/

//register write reponse
task automatic rxWr(int txId);/*{{{*/
  t_rspWr pkt;

  pkt.txId = txId;

  qRspWr.push_back(pkt);

  ->tick_rspWr; //wake a waitRspWr
endtask //rxWr/*}}}*/

//register read reponse
task automatic rxRd(int txId, logic [WIDTH_DATA-1:0] data[$]);/*{{{*/
  t_rspRd pkt;

  pkt.txId = txId;
  pkt.data = data;

  qRspRd.push_back(pkt);

  ->tick_rspRd;
endtask //rxRd/*}}}*/

//register write request
task automatic sendReqWr(input t_reqWr pkt);/*{{{*/
  string here;

  @(posedge clk);

  -> tick_reqWr;

  pkt.bAddrEnd   = pkt.bAddrStart
                 +(WIDTH_BYTE *pkt.data.size()) -1;

  here = $psprintf("%m");

  $display("[%s][NOTE] at %0d cycle, (id: %0d), write request, addr=0x%0x",
            here, cntClk, pkt.txId, pkt.bAddrStart);

  for(int i = 0; i < pkt.data.size(); i++) begin
    $display("[%s][NOTE] at %0d cycle, (id: %0d), data=0x%0x, strb=0x%0x",
              here, cntClk, pkt.txId, pkt.data[i], pkt.strb[i]);
  end //for i

  dut.txWr(
    .txId       (pkt.txId),
    .bAddrStart (pkt.bAddrStart),
    .bAddrEnd   (pkt.bAddrEnd),
    .data       (pkt.data),
    .strb       (pkt.strb)
  );
endtask //sendReqWr/*}}}*/

//handle a write reponse
task automatic waitRspWr();/*{{{*/
  t_rspWr pkt;

  @(tick_rspWr);

  pkt = qRspWr.pop_front();

  $display("[%m][NOTE] at %0d cycle, (id: %0d), write response", cntClk, pkt.txId);
endtask //waitRspWr/*}}}*/

//register read request
task automatic sendReqRd(input t_reqRd pkt);/*{{{*/
  @(posedge clk);

  -> tick_reqRd;

  $display("[%m][NOTE] at %0d cycle, (id: %0d), write request, addr=0x%0x to addr=x0x%0x",
                          cntClk, pkt.txId, pkt.bAddrStart, pkt.bAddrEnd);

  dut.txRd(
    .txId       (pkt.txId),
    .bAddrStart (pkt.bAddrStart),
    .bAddrEnd   (pkt.bAddrEnd)
  );

endtask //sendReqRd/*}}}*/

//handle a read reponse
task automatic waitRspRd(logic [WIDTH_DATA-1:0] expected[$]);/*{{{*/
  string here;

  t_rspRd pkt;

  @(tick_rspRd);

  pkt = qRspRd.pop_front();

  here = $psprintf("%m");

  $display("[%s][NOTE] at %0d cycle, (id: %0d), read response",
            here, cntClk, pkt.txId);

  foreach(pkt.data[i]) begin
    if(pkt.data[i] !== expected[i]) begin
      p_stimulus.cntFail++;

      $display("[%s][NOTE] at %0d cycle, (id: %0d), data=0x%0x while expected=0x%0x",
              here, cntClk, pkt.txId, pkt.data[i], expected[i]);
    end //if
    else begin
    $display("[%s][NOTE] at %0d cycle, (id: %0d), data=0x%0x",
              here, cntClk, pkt.txId, pkt.data[i]);
    end //else
  end //foreach
endtask //waitRspRd/*}}}*/

initial begin: p_stimulus
  string  nameTest;

  int cntTest = 0;    //test case number
  int cntFail = 0;    //number of test failure

  @(posedge rst_n);

  idle(10);

  `CASE("Single Write");
  begin/*{{{*/
    t_reqWr pkt;    //write request

    pkt.data.push_back('h1234);
    pkt.strb.push_back('b10);

    pkt.txId       = 0;
    pkt.bAddrStart = 'h100;

    fork
      sendReqWr(pkt);
      waitRspWr();
    join

    idle(100);
  end/*}}}*/

  `CASE("Single Read");
  begin/*{{{*/
    t_reqRd pkt;    //read request

    logic [WIDTH_DATA-1:0] exp[$];   //expected

    pkt.txId       = 1;
    pkt.bAddrStart = 'h100;
    pkt.bAddrEnd   = 'h108;

    exp.push_back('hxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_xxxx_12xx);

    fork
      sendReqRd(pkt);
      waitRspRd(exp);
    join

    idle(100);
  end/*}}}*/

  `CASE("Two Writes in a row");
  begin/*{{{*/
    t_reqWr pkt1;
    t_reqWr pkt2;

    pkt1.txId       = 2;                pkt2.txId       = 3;
    pkt1.bAddrStart = 'h200;            pkt2.bAddrStart = 'h300;
    pkt1.data.push_back('hABCD_ABCD);   pkt2.data.push_back('h4567_4567);
    pkt1.data.push_back('h4567_4569);   pkt2.data.push_back('hABCD_ABC0);
    pkt1.strb.push_back('b1111);        pkt2.strb.push_back('b1111);
    pkt1.strb.push_back('b1001);        pkt2.strb.push_back('b0110);

    fork
      begin
        sendReqWr(pkt1);
        sendReqWr(pkt2);
      end
      begin
        waitRspWr();
        waitRspWr();
      end
    join

    idle(100);
  end/*}}}*/

  `CASE("Two Reads in a row");
  begin/*{{{*/
    t_reqRd pkt1, pkt2;

    logic [WIDTH_DATA-1:0] exp1[$], exp2[$];  //expected

    pkt1.txId       = 4;                pkt2.txId = 5;
    pkt1.bAddrStart = 'h200;            pkt2.bAddrStart = 'h300;
    pkt1.bAddrEnd   = 'h21f;            pkt2.bAddrEnd   = 'h30F;

    exp1.push_back('hxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_abcd_abcd);  exp2.push_back('hxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_4567_4567);
    exp1.push_back('hxxxx_xxxx_xxxx_xxxx_xxxx_xxxx_45xx_xx69);

    fork
      begin
        sendReqRd(pkt1);
        sendReqRd(pkt2);
      end
      begin
        waitRspRd(exp1);
        waitRspRd(exp2);
      end
    join

    idle(100);
  end/*}}}*/

  `CASE("Single Read-prior-to-write");
  begin/*{{{*/
    t_reqRd pkt;    //read request

    logic [WIDTH_DATA-1:0] exp[$];   //expected

    pkt.txId       = 6;
    pkt.bAddrStart = 'h180;
    pkt.bAddrEnd   = 'h180;

    exp.push_back('hx);

    fork
      sendReqRd(pkt);
      waitRspRd(exp);
    join

    idle(100);
  end/*}}}*/

  `CASE("Backdoor Access");
  begin/*{{{*/
    longint addr = 'h300;

    byte data;             //   0      1      2      3     4     5     6     7
    byte expected[0:8-1] = {8'h67, 8'h45, 8'h67, 8'h45, 8'h0, 8'h0, 8'h0, 8'h0};

    for(int i=0; i < $size(expected); i++) begin
      data = dut.backdoorRd(addr +i);

      if(data != expected[i]) begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x, while expecting 0x%0x",
                               cntClk, addr +i, data, expected[i]);
        cntFail++;
      end //if
      else begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x",
                               cntClk, addr +i, data);
      end //else
    end //for i

    begin
      int  idx[0:2-1] = {   3,    5};
      byte tmp[0:2-1] = {'hAB, 'hEF};
      int  j;

      for(int i = 0; i < $size(idx); i++) begin
        j = idx[i];

        expected[j] = tmp[i];   dut.backdoorWr(addr +j, expected[j]);

        $display("[%m][NOTE] at %0d cycle, backdoorWr, addr=0x%0x, data=0x%0x",
                             cntClk, addr +j, expected[j]);
      end
    end

    for(int i=0; i < 8; i++) begin
      data = dut.backdoorRd(addr +i);

      if(data != expected[i]) begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x, while expecting 0x%0x",
                               cntClk, addr +i, data, expected[i]);
        cntFail++;
      end //if
      else begin
        $display("[%m][NOTE] at %0d cycle, backdoorRd, addr=0x%0x, data=0x%0x",
                               cntClk, addr +i, data);
      end //else
    end //for i

    idle(100);
  end/*}}}*/

  idle(100);

  if(cntFail) $display("[%m] ### Simulation End, Test case(s): %0d, RESULT: %0d FAIL(s) ###", cntTest, cntFail);
  else        $display("[%m] ### Simulation End, Test case(s): %0d, RESULT: PASS ###", cntTest);
  $finish();
end //p_stimulus

`undef  CASE
`undef  IDLE
/*}}}*/

`undef __TESTBENCH_NAME__
endmodule //tb_busSubordinateModel
/*}}}*/
//synopsys translate_on

`endif//__EN_BUILT_IN_TEST_BENCH__
`endif//SYNTHESIS
/*}}}*/

//vim60:fdm=marker
//vim60:ts=4
//vim60:sw=4
`endprotect128
