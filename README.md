# Description

  This repository represents onboarding template.

  This Template support to do onboarding for digital IP block which has 1 axi4 slave and 1 apb slave bus interface, maximum 4 interrupt outputs, and 32-bits in/out/inout ports to connected with vip.

***


# How to configure SCU(System Control Unit) structure in the onboard system

The SoC design created when configuring the workspace with this repository has an scu structure as shown in the following figure.

![](./images/onboardsystem_scu_structure_20211021.png)

If you would like to see a larger and more detailed view of the above diagram, you can open the link in the 'scu_diagram_link_20211021.txt' file. Note that you may need to install draw.io to open this link.

The SoC for this template has three subsystems, and the mainbussystem(skeletonsystem) becomes the Main Subsystem with the Main scu.
The source clock input from outside the SoC passes through this Main scu to the other subSystems.
Several clock groups can be generated in the scu, and the clocks input from the outside of the subsystem and the clocks output from the PLL inside the scu are used as the source clocks of the clock groups. The clocks generated from the clock groups can be used for bus or device inside the subsystem or output to the outside of the subsystem and transmitted to oterh subsystems.

Since this repository is for onboarding template, this page only describes the scu configuration of the onboardsystem.

SCU configurations for the onboardsystem can be set in devicetopology for the onboardsystem in the **soc-testsocket-selective** repository.
Also, which clock to connect to the target IP to be onboard can be set in chisel wrapper(template wrapper) of this repository(**block-template_a1-selective**).

SCU configurations of the onboardsystem may be classified as follows. User can make more detailed description by directly modifying the Scala code, but this guide can also satisfy the clock management policy.


1. Declare the input clocks of the scu in the onboardsystem and the clock groups to be generated inside the scu.

   ```scala
   // ~/soc-testsocket-semifive/src/skeleton/config/onboardsystem/DeviceTopology.scala
   ...
   case DeviceTopologySite(InOnboardSystem) => Seq(DeviceTopology(
     place = Seq(
       AXI4SCUPlaceParams(
         key = AXI4SCUSite,
         params = AXI4SCUParams(
           base = BigInt(0x90022000L),
           busWidthBytes = 8,
           iportParams = Seq(
             SCUCRGAttachParams("sharedClock0", 400) // (1) in the above figure. This clock has the highest priority to the mux selection. this clock is selected when mux selection is 2'b00 or 2'b11 (top priority clock is selected when else case is selected)
           ),
           plls = Seq(
             SCUPLLParams(
               iportParams = Seq(
                 SCUCRGAttachParams("sharedClock1", 100) // (2) in the above figure. This clock is selected when mux selection is 2'b01
               )
             )
           ),
           clockgroups = Seq(
             SCUClockgroupParams("inbound_PBUS", 200), // (4) in the above figure
             SCUClockgroupParams(
               "inbound_IP", 50, // (5) in the above figure
               iportParams = Seq(
                 SCUCRGAttachParams("dedicatedClock0", 50) // (3) in the above figure. This clock is selected when mux selection is 2'b10
               )
             )
           )
         ),
         controlWhere = AXI4PBUS,
         interruptWhere = IBUS
       ))))
   ...

   // ~/soc-testsocket-semifive/src/skeleton/config/onboardsystem/BusTopology.scala
   ...
   case BusTopologySite(InOnboardSystem) => Seq(BusTopology(
     place = Seq(
       AXI4FullXbarBusPlaceParams(
         key = AXI4PBUS,
         params = AXI4BusWrapperParams(
           name = "AXI4_PBUS",
           crgAttachParams = SCUCRGAttachParams("axi4_pbus", 200, Some("inbound_PBUS")), // (6) in the above figure
           fixedClockOpt = None)),
       InterruptBusPlaceParams(
         key = IBUS,
         params = InterruptBusWrapperParams(name = "IBUS")))))
   ...
   class WithAPBFanoutBusSlaveInOnboardSystem(
       key: APBBusSite = APBPBUS,
       params: APBBusWrapperParams = APBBusWrapperParams(
         name = "APB_PBUS",
         crgAttachParams = SCUCRGAttachParams("apb_pbus", 200, Some("inbound_PBUS")), // (7) in the above figure
         fixedClockOpt = None),
       from: AXI4BusSite = AXI4PBUS,
       fromSystem: SystemSite = InOnboardSystem,
       devNullParams: DevNullParams = DevNullParams(
         address = List(AddressSet(0x6000, 0xfff)),
         maxAtomic = 8,
         maxTransfer = 4096))
   ...

   // ~/block-template_a1-semifive/craft/templatewrapper.scala
   ...
   class WithTemplateWrapper extends Config((site, here, up) => {
     case DeviceTopologySite(InOnboardSystem) =>
       DeviceTopology(
         place = Seq(
           TemplateWrapperPlaceParams(
             key = TemplateWrapperSite(0),
             params = NtemplatewrapperTopParams.defaults(0x90000000L, 0x90001000L, site(CacheBlockBytes), SCUCRGAttachParams("templatewrapper", 50, Some   ("inbound_IP"))), // (8) in the above figure
             controlWhere = APBPBUS,
             dataWhere = AXI4PBUS,
             interruptWhere = IBUS))
       ) +: up(DeviceTopologySite(InOnboardSystem))
   })
   ...
   ```

A clock group consists of a combination of a cmdWrapper(clock mux divider wrapper) and crgWrappers(clock reset generation wrapper), and multiple crgWrappers are connected to the cmdWrapper. Since all clocks output from the clock group go through crgWrapper, the number of crgWrapper determines the number of clocks output. These clocks are all in the same clock domain.

Declaring a clock group means setting the name and clock frequency of this clock group. The name of the clock group is referenced when connecting clocks between subSystems at a higher level of subSystem. The clock frequency is the maximum operating frequency available to the corresponding clock group and can be divided by N through the SFR control. This frequency does not have any redundancy with others, simply indicating the frequency information of the corresponding clock group in the code. However, if the clock output from the clock group is output outside the system, the same clock name and frequency information **MUST** be configured in order to connect to the other subsystems at the upper level.

The input clocks of the scu is divided into **'sharedClock{N}'** connected to all cmdWrappers and **'dedicatedClock{N}'** connected only to a specific cmdWrapper. 'N' is the index indicating the clock input order. The prefix **'inbound_'** is applied to the name of the clock group that generates the clock to be used inside the onboardsystem. It means it is used inside the subsystem.

For reference, Mux and Divider are sequentially connected in the cmdWrapper. And clocks that are directly input to the Mux in the cmdWrapper from outside the subsystem are connected in order to the input pins with high priority of the Mux in the order of declaration. After that, the clock input to the Mux through the PLL wrapper is connected to the next-order input pin of the Mux.


2. Create clock groups to the scu in mainbussystem. The clocks output from this clock group are input to the scu in the onboardsystem.

   ```scala
   // ~/soc-testsocket-semifive/src/skeleton/config/onboardsystem/DeviceTopology.scala
   ...
   case DeviceTopologySite(InSkeletonSystem) => {
     up(DeviceTopologySite(InSkeletonSystem)).map {
       dt => {
         dt.copy(place = dt.place.map{
                   case sp: AXI4SCUPlaceParams => {
                     sp.copy(
                       params = sp.params.copy(
                         clockgroups = sp.params.clockgroups ++ Seq(
                           SCUClockgroupParams(
                             "outbound_ONBOARDSYSTEM0", 400,                                         // (9) in the above figure
                             oportParams = Seq(SCUCRGAttachParams("outbound_ONBOARDSYSTEM0", 400))), // This clock can be exported to the top level, and can be connected to other subsystems
                           SCUClockgroupParams(
                             "outbound_ONBOARDSYSTEM1", 100,                                         // (10) in the above figure
                             oportParams = Seq(SCUCRGAttachParams("outbound_ONBOARDSYSTEM1", 100))), // This clock can be exported to the top level, and can be connected to other subsystems
                           SCUClockgroupParams(
                             "outbound_ONBOARDSYSTEM2", 50,                                          // (11) in the above figure
                             oportParams = Seq(SCUCRGAttachParams("outbound_ONBOARDSYSTEM2", 50)))   // This clock can be exported to the top level, and can be connected to other subsystems
                         )
                       )
                     )
                   }
                   case other@_ => other
                 }
         )
       }
     }
   }
   ...
   ```

Clock groups to be used by mainbussystem or clock groups to be transmitted to coresystem are already configured In devicetopology of mainbussystem. So, Add configuration to create clock groups that will be connected from the mainbussystem to the onboardsystem.

Apply the prefix 'outbound_' to the clock groups name. This means that the clock groups are output outside the mainbussystem.


3. Declare the clock connection between subSystems.

   ```scala
   // ~/soc-testsocket-semifive/src/skeleton/config/onboardsystem/DeviceTopology.scala
   ...
   case DeviceTopologySite(InSkeletonSoC) =>
     DeviceTopology(
       connect = Seq(
         AXI4SCUConnectParams( // (12) in the above figure
           from = AXI4SCUSite, fromSystem = Some(InSkeletonSystem), clockgroupNames = Seq("outbound_ONBOARDSYSTEM0"),
           to = AXI4SCUSite, toSystem = Some(InOnboardSystem), crgAttachParams = Seq(SCUCRGAttachParams("sharedClock0", 400))),
         AXI4SCUConnectParams( // (13) in the above figure
           from = AXI4SCUSite, fromSystem = Some(InSkeletonSystem), clockgroupNames = Seq("outbound_ONBOARDSYSTEM1"),
           to = AXI4SCUSite, toSystem = Some(InOnboardSystem), crgAttachParams = Seq(SCUCRGAttachParams("sharedClock1", 100))),
         AXI4SCUConnectParams( // (14) in the above figure
           from = AXI4SCUSite, fromSystem = Some(InSkeletonSystem), clockgroupNames = Seq("outbound_ONBOARDSYSTEM2"),
           to = AXI4SCUSite, toSystem = Some(InOnboardSystem), crgAttachParams = Seq(SCUCRGAttachParams("dedicatedClock0", 50)))
       )
     ) +: up(DeviceTopologySite(InSkeletonSoC))
   ...
   ```

The clock connection between the mainbussystem and the coresystem is already declared in the soc top level(SkeletonSoC) devicetopology of the **soc-testsocket-semifive** repository. Therefore, just add a declaration for the clock connection between mainbussystem and onboardsystem in onboardsystem's devicetopology. The same name and frequency information of the clock group declared inside each subsystem **MUST** be used when you connect clocks in the soc level. However, in the case of a system that delivers a clock groups, frequency information does not have to be specified.


# How to create workspace and build rtl, run simulation

1. Create workspace

   ```bash
   # prepare required tool chains, wit & wake
   # this is an example in SEMIFIVE, and assuming user run this on the SEMIFIVE uranus server where intra-network.

   module load sifive/wit/0.14.1 module load sifive/wake/0.20.1
   ```

   ```bash
   # initialize workspace

   wit init {WORKSPACE_NAME} -a git@portal:league/block-template_a1-semifive.git -a git@portal:league/environment-standalone.git
   ```

2. Run build and simulation

   ```bash
   wake --init .

   # running a single/regression test with the target json file and additional options
   #   - wake -x  'runSimWithV3 $dut $testType $ciTypeOrTestName $jsonPath $simOptions'
   #
   # you can see more details can be checked in the README.md of the api-generator-semifive repository
   #
   srun wake -vx 'runSimWithV3 templatewrapperSoC "regression" "nightly"   "block-template_a1-semifive/tests/json/templatewrapper.json" (VCS,Waves,SemiCPU,Nil)' 2>&1 | tee run.log
   srun wake -vx 'runSimWithV3 templatewrapperSoC "single"     "testcase2" "block-template_a1-semifive/tests/json/templatewrapper.json" (VCS,Waves,SemiCPU,Nil)' 2>&1 | tee run.log

   # these are recommended commands for more convenience
   #   - wake -x  'runRegressionSimWithRiscv $dut $testType $jsonPath'
   #
   # you can see more details can be checked in the README.md of the api-generator-semifive repository
   #
   # Warning: User must configure json file in the tests directory to choose between riscv and semicpu as a host and riscv is not supported yet
   #
   srun wake -vx 'runRegressionSimWoWave          templatewrapperSoC "premr"     "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWoWave          templatewrapperSoC "postmr"    "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWoWave          templatewrapperSoC "nightly"   "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWoWave          templatewrapperSoC "all"       "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWithWave        templatewrapperSoC "premr"     "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWithWave        templatewrapperSoC "postmr"    "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWithWave        templatewrapperSoC "nightly"   "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runRegressionSimWithWave        templatewrapperSoC "all"       "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWoWave              templatewrapperSoC "testcase1" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWoWave              templatewrapperSoC "testcase2" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWoWave              templatewrapperSoC "testcase3" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWithWave            templatewrapperSoC "testcase1" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWithWave            templatewrapperSoC "testcase2" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   srun wake -vx 'runSingleSimWithWave            templatewrapperSoC "testcase3" "block-template_a1-semifive/tests/json/templatewrapper.json"' 2>&1 | tee run.log
   ```

3. Change repository for your own onboarding works.
   Replace template keyword to the name of the target IP you want to do onboarding.
   Please refer as follow example:

   ```bash
   # first of all, you need to make sure you are in the root of this workspace.
   # change the same of the repository

   mv ./block-template_a1-semifive ./block-gpio-semifive
   ```

   ```bash
   # go in to the onboarding repository

   cd ./block-gpio-semifive
   ```

   ```bash
   # change directory name

   find ./ -name "template*" | sed -e 'p' -e 's/template/gpio/g' | xargs -n 2 mv
   find ./ -name "template*" | sed -e 'p' -e 's/template/gpio/g' | xargs -n 2 mv
   ```

   ```bash
   # change the keyword in the files.

   $ find ./ -type f -exec sed -i -e 's/template_a1/gpio/g' {} \;
   $ find ./ -type f -exec sed -i -e 's/template/gpio/g' {} \;
   ```

   ```bash
   # remove git information, and make new local git repository

   rm -rf .git
   git init
   git add .
   cd ../
   ```

   ```bash
   # run build & simulation referring to the commands described in above after wake initialization
   ```

   ```bash
   # commit your repository, and push to the remote
   ```


# tests directory structure

1. Users only need to update the test codes and specific json file for onboarding tests basically.
   Please refre thehe following tree structure of the 'tests' sub directory.
   This is for user configuring tests

   ```bash
   tests
       ├── c
       │   ├── common                 : shared s/w files
       │   │   └── include
       │   │       └── map_onboard.h
       │   ├── testcase1              : s/w files for testcase1
       │   │   ├── Makefile
       │   │   ├── testcase1.cpp
       │   │   └── testcase1.h
       │   ├── testcase2              : s/w files for testcase1
       │   │   ├── Makefile
       │   │   ├── testcase2.cpp
       │   │   └── testcase2.h
       │   └── testcase3              : s/w files for testcase2
       │       ├── Makefile
       │       ├── testcase3.cpp
       │       └── testcase3.h
       └── json                       : json file contains of the test options
           └── templatewrapper.json
   ```

2. Links to reference for unity

* [Unity Project](https://github.com/ThrowTheSwitch/Unity)
* [Unity Getting Started Guide](https://github.com/ThrowTheSwitch/Unity/blob/master/docs/UnityGettingStartedGuide.md)


# Continuous Integration for Merge Request

You have to pass the CI before merging your MR.
