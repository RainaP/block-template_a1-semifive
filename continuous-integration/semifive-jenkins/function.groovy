def configuration() {
    env.project             = "block-template_a1-semifive"
    env.group               = "league"
    env.gitlab              = "portal"
    env.environment         = "environment-standalone"
    env.module              = "environment.sh"
}

def variable(){
    source_branch       = "$gitlabSourceBranch"
    target_branch       = "$gitlabTargetBranch"
}

def path(){
    archivePath = "/home/share/jenkins/archive"
    ciPath = "/continuous-integration/semifive-jenkins"
}

def getGitBranchName(){
    return sh(returnStdout: true, script: """echo "${scm.branches[0].name}" | awk -F "/" '{print \$2}'""").trim()
}

def checkBranch(){
    default_branch = function.getGitBranchName()
    println(default_branch)

    if(target_branch != default_branch){
        sh "exit 0"
    }
}

def defaultBranch(){
    default_branch = "master"
}

def gitClone(){
    sh "git clone git@${env.gitlab}:${env.group}/${env.project}.git -b ${default_branch}"
}

def gitMerge(){
    sh "git -C ${env.project} merge origin/${target_branch} origin/${source_branch}"
}

def witUpdate(){
    sh """
        source ${env.project}/${ciPath}/${env.module}
        wit init .
        wit add-pkg git@${env.gitlab}:${env.group}/${env.project}.git
        wit add-pkg git@${env.gitlab}:${env.group}/${env.environment}.git
        wit update-pkg ${env.project}
        wit update
    """
}

def runSim(){
    tee("full.log"){
        sh """
            source ${env.project}/${ciPath}/${env.module}
            wake --init .
            source ${env.project}/${ciPath}/wake-command.sh
            """
    }
}

def archive(){
    try{
        if (fileExists("${archivePath}/${JOB_NAME}") == false){
            sh "mkdir -p ${archivePath}/${JOB_NAME}"
        }
        tmp_pwd = sh(script: "pwd", returnStdout:true).trim()
        sh "nice tar -czf ${archivePath}/${JOB_NAME}/${JOB_NAME}.${BUILD_NUMBER}.tar.gz --ignore-failed-read --exclude=.fuse ${tmp_pwd}"
    }catch(e){
        println("archive error")
    }
}
return this
